<?php

namespace DealTrak\Model\Rosetta;

use DealTrak\Model\Rosetta\Business\Business;
use DealTrak\Model\Rosetta\Business\Director;
use DealTrak\Model\Rosetta\Business\OrganisationBank;
use DealTrak\Model\Rosetta\Business\OrganisationDetails;
use DealTrak\Model\Rosetta\Business\OrganisationSection;
use DealTrak\Model\Rosetta\Personal\ApplicantDetailsSection;
use DealTrak\Model\Rosetta\Personal\ApplicantSection;
use DealTrak\Model\Rosetta\Personal\Application;
use JMS\Serializer\Annotation as JMS;

/**
 * Class PersonalRosetta
 *
 * @package DealTrak\Model\Rosetta
 * @JMS\XmlRoot("rosetta")
 */
class Rosetta
{
    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\XmlAttribute
     * @JMS\Groups({"personal", "business"})
     */
    public $live;

    /**
     * @var Routing
     * @JMS\Type("DealTrak\Model\Rosetta\Routing")
     * @JMS\Groups({"personal", "business"})
     */
    public $routing;

    /**
     * @var Actions
     * @JMS\Type("DealTrak\Model\Rosetta\Actions")
     * @JMS\SkipWhenEmpty
     * @JMS\Groups({"personal", "business"})
     */
    public $actions;

    /**
     * @var Application
     * @JMS\Type("DealTrak\Model\Rosetta\Personal\Application")
     * @JMS\Groups({"personal"})
     */
    public $application = null;

    /**
     * @var Business
     * @JMS\Type("DealTrak\Model\Rosetta\Business\Business")
     * @JMS\Groups({"business"})
     */
    public $business = null;

    /**
     * @param int           $live
     * @param Routing       $routing
     * @param Actions|null  $actions
     * @param Application   $application
     * @param Business|null $business
     */
    public function __construct(
        int $live = 0,
        ?Routing $routing = null,
        ?Actions $actions = null,
        ?Application $application = null,
        ?Business $business = null
    ) {
        $this->live        = $live;
        $this->routing     = $routing;
        $this->actions     = $actions;
        $this->application = $application;
        $this->business    = $business;
    }

    /**
     * Returns true of the Rosetta application is a business application.
     *
     * @return bool
     */
    public function isBusiness()
    {
        return !empty($this->business);
    }

    /**
     * Returns the first applicant's details section.
     *
     * @return ApplicantDetailsSection[]
     */
    public function applicants()
    {
        return $this->application->applicantSection;
    }

    /**
     * Returns the first applicant's details section.
     *
     * @return ApplicantSection
     */
    public function firstApplicant()
    {
        return $this->application->applicantSection[0];
    }

    /**
     * @return VehicleSection
     */
    public function vehicle()
    {
        $section = $this->application ?: $this->business;

        return $section->vehicleSection;
    }

    /**
     * @return FinanceSection
     */
    public function finance()
    {
        $section = $this->application ?: $this->business;

        return $section->financeSection;
    }

    /**
     * @return ExtrasSection
     */
    public function extras()
    {
        $section = $this->application ?: $this->business;

        return $section->extrasSection;
    }

    /**
     * @return OrganisationSection
     */
    public function organisation()
    {
        return $this->business->organisationSection;
    }

    /**
     * Returns the business organisation details.
     *
     * @return OrganisationDetails
     */
    public function organisationDetails()
    {
        return $this->business->organisationSection->organisationDetails;
    }

    /**
     * Returns the business organisation details.
     *
     * @return OrganisationBank
     */
    public function organisationBank()
    {
        return $this->business->organisationSection->organisationBank;
    }

    /**
     * @return Director
     */
    public function firstDirector()
    {
        return $this->business->directorSection->director[0];
    }

    /**
     * @return array|Director[]
     */
    public function directors()
    {
        return $this->business->directorSection->director;
    }
}