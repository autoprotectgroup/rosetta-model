<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Note
 *
 * @package DealTrak\Model\Rosetta
 */
class Note
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     * @JMS\Inline
     */
    public $note;

    /**
     * @param string $note
     */
    public function __construct(string $note)
    {
        $this->note = $note;
    }
}