<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Vaps
 *
 * @package DealTrak\Model\Rosetta
 */
class Vaps
{
    /**
     * @var Vap[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\Vap>")
     * @JMS\XmlList(inline=true, entry="vap")
     * @JMS\Groups({"personal", "business"})
     */
    public $vaps;

    /**
     * @param Vap[] $vaps
     */
    public function __construct(array $vaps = [])
    {
        $this->vaps = $vaps;
    }
}