<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Notes
 *
 * @package DealTrak\Model\Rosetta
 */
class Notes
{
    /**
     * @var Note[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\Note>")
     * @JMS\XmlList(inline=true, entry="note")
     * @JMS\Groups({"personal", "business"})
     */
    public $notes;

    /**
     * @param Note[] $notes
     */
    public function __construct(array $notes = [])
    {
        $this->notes = $notes;
    }
}