<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class VehicleSection
 *
 * @package DealTrak\Model\Rosetta
 */
class VehicleSection
{
    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("newused")
     * @JMS\Groups({"personal", "business"})
     */
    public $newUsed;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $make;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $model;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $engineSize;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $style;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $assetTypeId;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $description;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $doors;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $regNo;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $firstReg;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $vin;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("capcode")
     * @JMS\Groups({"personal", "business"})
     */
    public $capCode;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $tradeValue;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $retailValue;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $glassesCode;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SkipWhenEmpty
     * @JMS\Groups({"personal", "business"})
     */
    public $glassesCodeFamily;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SkipWhenEmpty
     * @JMS\Groups({"personal", "business"})
     */
    public $colour;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $fuel;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $transmission;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $usage;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $yearMan;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $import;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $mileage;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $stockNumber;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SkipWhenEmpty
     * @JMS\Groups({"personal", "business"})
     */
    public $deliveryDate;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $type;

    /**
     * @param int            $newUsed
     * @param string         $make
     * @param string         $model
     * @param int            $engineSize
     * @param string         $style
     * @param int|null       $assetTypeId
     * @param string         $description
     * @param int            $doors
     * @param string         $regNo
     * @param string         $firstReg
     * @param string         $vin
     * @param string         $capCode
     * @param string         $tradeValue
     * @param string         $retailValue
     * @param string         $glassesCode
     * @param string         $glassesCodeFamily
     * @param string         $colour
     * @param int            $fuel
     * @param int            $transmission
     * @param int            $usage
     * @param string         $yearMan
     * @param int            $import
     * @param int            $mileage
     * @param int|null       $stockNumber
     * @param string|null    $deliveryDate
     * @param int            $type
     */
    public function __construct(
        int $newUsed,
        string $make,
        string $model,
        string $style,
        string $description,
        string $regNo,
        string $vin,
        string $capCode,
        string $tradeValue,
        string $retailValue,
        string $glassesCode,
        string $glassesCodeFamily,
        string $colour,
        int $fuel,
        int $transmission,
        int $usage,
        int $import,
        int $mileage,
        int $type,
        ?int $engineSize,
        ?int $doors,
        ?string $yearMan,
        ?int $assetTypeId = null,
        ?string $firstReg = null,
        ?int $stockNumber = null,
        ?string $deliveryDate = null
    ) {
        $this->newUsed = $newUsed;
        $this->make = $make;
        $this->model = $model;
        $this->engineSize = $engineSize;
        $this->style = $style;
        $this->assetTypeId = $assetTypeId;
        $this->description = $description;
        $this->doors = $doors;
        $this->regNo = $regNo;
        $this->firstReg = $firstReg;
        $this->vin = $vin;
        $this->capCode = $capCode;
        $this->tradeValue = $tradeValue;
        $this->retailValue = $retailValue;
        $this->glassesCode = $glassesCode;
        $this->glassesCodeFamily = $glassesCodeFamily;
        $this->colour = $colour;
        $this->fuel = $fuel;
        $this->transmission = $transmission;
        $this->usage = $usage;
        $this->yearMan = $yearMan;
        $this->import = $import;
        $this->mileage = $mileage;
        $this->stockNumber = $stockNumber;
        $this->deliveryDate = $deliveryDate;
        $this->type = $type;
    }

    /**
     * @JMS\PreSerialize
     */
    public function onPreSerialize()
    {
        $this->yearMan     = $this->yearMan === null     ? '' : $this->yearMan;
        $this->doors       = $this->doors === null       ? '' : $this->doors;
        $this->engineSize  = $this->engineSize === null  ? '' : $this->engineSize;
        $this->assetTypeId = $this->assetTypeId === null ? '' : $this->assetTypeId;
    }
}
