<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class EmploymentSection
 *
 * @package DealTrak\Model\Rosetta
 */
class EmploymentSection
{
    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\XmlAttribute
     * @JMS\Groups({"personal", "business"})
     */
    public $sequence;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $occupation;

    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $occupationId;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $company;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $building;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("subbuilding")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $subBuilding;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $street;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $district;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("towncity")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $townCity;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $county;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $postcode;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $years;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $months;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $employmentStatus;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $employmentSector;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $employmentType;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $grossIncome;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $income;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $incomeFreq;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $ukTradingAddress;

    /**
     * @param int         $sequence
     * @param string      $occupation
     * @param int|null    $occupationId
     * @param string      $company
     * @param string      $building
     * @param string|null $subBuilding
     * @param string|null $street
     * @param string|null $district
     * @param string      $townCity
     * @param string|null $county
     * @param string|null $postcode
     * @param int         $years
     * @param int         $months
     * @param int         $employmentStatus
     * @param int         $employmentSector
     * @param int         $employmentType
     * @param string|null $grossIncome
     * @param string|null $income
     * @param int         $incomeFreq
     * @param int         $ukTradingAddress
     */
    public function __construct(
        int $sequence,
        string $occupation,
        int $occupationId,
        string $company,
        string $building,
        string $townCity,
        int $years,
        int $months,
        int $employmentStatus,
        int $employmentSector,
        int $employmentType,
        int $incomeFreq,
        int $ukTradingAddress,

        string $subBuilding = '',
        string $street = '',
        string $district = '',
        string $county = '',
        string $postcode = '',
        string $grossIncome = '',
        string $income = ''
    ) {
        $this->sequence = $sequence;
        $this->occupation = $occupation;
        $this->occupationId = $occupationId;
        $this->company = $company;
        $this->building = $building;
        $this->subBuilding = $subBuilding;
        $this->street = $street;
        $this->district = $district;
        $this->townCity = $townCity;
        $this->county = $county;
        $this->postcode = $postcode;
        $this->years = $years;
        $this->months = $months;
        $this->employmentStatus = $employmentStatus;
        $this->employmentSector = $employmentSector;
        $this->employmentType = $employmentType;
        $this->grossIncome = $grossIncome;
        $this->income = $income;
        $this->incomeFreq = $incomeFreq;
        $this->ukTradingAddress = $ukTradingAddress;
    }

    /**
     * @JMS\PreSerialize
     */
    public function onPreSerialize()
    {
        $this->occupationId = $this->occupationId === 0 ? '' : $this->occupationId;
    }
}