<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Cpi
 *
 * @package DealTrak\Model\Rosetta
 */
class Cpi
{
    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $jointCover;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $productCode;

    /**
     * @param int    $jointCover
     * @param string $productCode
     */
    public function __construct(
        int $jointCover,
        string $productCode
    ) {
        $this->jointCover = $jointCover;
        $this->productCode = $productCode;
    }
}