<?php

namespace DealTrak\Model\Rosetta\Personal;

use DealTrak\Model\Rosetta\ExtrasSection;
use DealTrak\Model\Rosetta\FinanceSection;
use DealTrak\Model\Rosetta\NoVehicleYetSection;
use DealTrak\Model\Rosetta\VehicleSection;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Application
 *
 * @package DealTrak\Model\Rosetta\Personal
 */
class Application
{
    /**
     * @var ApplicantSection[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\Personal\ApplicantSection>")
     * @JMS\XmlList(inline=true, entry="applicant_section")
     * @JMS\Groups({"personal"})
     */
    public $applicantSection;

    /**
     * @var VehicleSection
     * @JMS\Type("DealTrak\Model\Rosetta\VehicleSection")
     * @JMS\Groups({"personal"})
     */
    public $vehicleSection;

    /**
     * @var NoVehicleYetSection
     * @JMS\Type("DealTrak\Model\Rosetta\NoVehicleYetSection")
     * @JMS\SerializedName("vehicle_section")
     * @JMS\Groups({"personal"})
     */
    public $noVehicleYetSection;

    /**
     * @var FinanceSection
     * @JMS\Type("DealTrak\Model\Rosetta\FinanceSection")
     * @JMS\Groups({"personal"})
     */
    public $financeSection;

    /**
     * @var ExtrasSection
     * @JMS\Type("DealTrak\Model\Rosetta\ExtrasSection")
     * @JMS\Groups({"personal"})
     */
    public $extrasSection;

    /**
     * @param array|ApplicantSection[] $applicantSection
     * @param VehicleSection|null      $vehicleSection
     * @param NoVehicleYetSection|null $noVehicleYetSection
     * @param FinanceSection           $financeSection
     * @param ExtrasSection            $extrasSection
     */
    public function __construct(
        array $applicantSection,
        FinanceSection $financeSection,
        ExtrasSection $extrasSection,
        ?VehicleSection $vehicleSection = null,
        ?NoVehicleYetSection $noVehicleYetSection = null,
    ) {
        if ($vehicleSection !== null && $noVehicleYetSection !== null) {
            throw new \LogicException('Only one type of vehicle section can exist');
        }

        $this->applicantSection = $applicantSection;
        $this->vehicleSection = $vehicleSection;
        $this->noVehicleYetSection = $noVehicleYetSection;
        $this->financeSection = $financeSection;
        $this->extrasSection = $extrasSection;
    }

    /**
     * @return ApplicantDetailsSection
     */
    public function firstApplicant()
    {
        return $this->applicantSection[0];
    }

    /**
     * @return array
     */
    public function applicants()
    {
        return $this->applicantSection;
    }

    /**
     * @param ApplicantSection $applicantSection
     */
    public function addApplicantSection(ApplicantSection $applicantSection)
    {
        $this->applicantSection[] = $applicantSection;
    }

    /**
     * @return VehicleSection
     */
    public function vehicle()
    {
        return $this->vehicleSection;
    }

    /**
     * @return FinanceSection
     */
    public function finance()
    {
        return $this->financeSection;
    }

    /**
     * @return ExtrasSection
     */
    public function extras()
    {
        return $this->extrasSection;
    }
}
