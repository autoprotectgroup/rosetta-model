<?php

namespace DealTrak\Model\Rosetta\Personal;

use JMS\Serializer\Annotation as JMS;

/**
 * Class ApplicantDetailsSection
 *
 * @package DealTrak\Model\Rosetta\Personal
 */
class ApplicantDetailsSection
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $title;

    /**
     * @var
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $forename;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $middlename;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $surname;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $alias;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SkipWhenEmpty
     * @JMS\Groups({"personal"})
     */
    public $maidenName;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $dob;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $gender;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $homePhone;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $mobile;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $workPhone;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $email;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $dependants;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $maritalStatus;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $passport;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $drivingLicense;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $drivingLicenseNumber;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $nationality;

    /**
     * @var int|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $relationship;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $distanceMarketed;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $optinPost;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $optinEmail;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $optinPhone;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $optinSms;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $ukResident;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $pension = null;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $creditEnquiry = null;

    /**
     * @var Aml
     * @JMS\Type("DealTrak\Model\Rosetta\Personal\Aml")
     * @JMS\Groups({"personal"})
     */
    public $aml = null;

    /**
     * @param string $title
     * @param string $forename
     * @param string|null $middlename
     * @param string $surname
     * @param string|null $alias
     * @param string|null $maidenName
     * @param string|null $dob
     * @param string $gender
     * @param string $homePhone
     * @param string|null $mobile
     * @param string|null $workPhone
     * @param string|null $email
     * @param int $dependants
     * @param int $maritalStatus
     * @param int|null $passport
     * @param int|null $drivingLicense
     * @param string|null $drivingLicenseNumber
     * @param int $nationality
     * @param int|null $relationship
     * @param int $distanceMarketed
     * @param int $optinPost
     * @param int $optinEmail
     * @param int $optinPhone
     * @param int $optinSms
     * @param int $ukResident
     * @param int|null $pension
     * @param int|null $creditEnquiry
     * @param Aml|null $aml
     */
    public function __construct(
        string $title,
        string $forename,
        string $surname,
        string $gender,
        string $homePhone,
        int $dependants,
        int $maritalStatus,
        int $nationality,
        int $distanceMarketed,
        int $optinPost,
        int $optinEmail,
        int $optinPhone,
        int $optinSms,
        int $ukResident,
        ?string $middlename = '',
        ?string $alias = '',
        ?string $maidenName = '',
        ?string $dob = null,
        ?string $mobile = '',
        ?string $workPhone = '',
        ?string $email = '',
        ?int $passport = null,
        ?int $drivingLicense = null,
        ?string $drivingLicenseNumber = '',
        ?int $relationship = null,
        ?int $pension = null,
        ?int $creditEnquiry = null,
        ?Aml $aml = null
    ) {
        $this->title = $title;
        $this->forename = $forename;
        $this->middlename = $middlename;
        $this->surname = $surname;
        $this->alias = $alias;
        $this->maidenName = $maidenName;
        $this->dob = $dob;
        $this->gender = $gender;
        $this->homePhone = $homePhone;
        $this->mobile = $mobile;
        $this->workPhone = $workPhone;
        $this->email = $email;
        $this->dependants = $dependants;
        $this->maritalStatus = $maritalStatus;
        $this->passport = $passport;
        $this->drivingLicense = $drivingLicense;
        $this->drivingLicenseNumber = $drivingLicenseNumber;
        $this->nationality = $nationality;
        $this->relationship = $relationship;
        $this->distanceMarketed = $distanceMarketed;
        $this->optinPost = $optinPost;
        $this->optinEmail = $optinEmail;
        $this->optinPhone = $optinPhone;
        $this->optinSms = $optinSms;
        $this->ukResident = $ukResident;
        $this->pension = $pension;
        $this->creditEnquiry = $creditEnquiry;
        $this->aml = $aml;
    }

    /**
     * @JMS\PreSerialize
     */
    public function onPreSerialize()
    {
        $this->passport = $this->passport === 0 ? '' : $this->passport;
    }
}