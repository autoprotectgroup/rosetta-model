<?php

namespace DealTrak\Model\Rosetta\Personal;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Aml
 *
 * @package DealTrak\Model\Rosetta\Personal
 */
class Aml
{
    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $currentlyOnEr;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $numberOfOpenInsightLenders;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $currentlyBtListed;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $numberOfCcjs;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $totalProofsOfResidency;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $dateOfBirthMatchOnInsight;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $dateOfBirthMatchOnErAttainerDate;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $homePhoneMatch;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $totalProofsOfIdentity;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $onBoe;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $onPep;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $onHalo;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $onOns;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $onOfac;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $onCifas;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $linkedForwardingAddress;

    /**
     * @var int0
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $totalAlerts;

    /**
     * @param int $currentlyOnEr
     * @param int $numberOfOpenInsightLenders
     * @param int $currentlyBtListed
     * @param int $numberOfCcjs
     * @param int $totalProofsOfResidency
     * @param int $dateOfBirthMatchOnInsight
     * @param int $dateOfBirthMatchOnErAttainerDate
     * @param int $homePhoneMatch
     * @param int $totalProofsOfIdentity
     * @param int $onBoe
     * @param int $onPep
     * @param int $onHalo
     * @param int $onOns
     * @param int $onOfac
     * @param int $onCifas
     * @param int $linkedForwardingAddress
     * @param int $totalAlerts
     */
    public function __construct(
        $currentlyOnEr,
        $numberOfOpenInsightLenders,
        $currentlyBtListed ,
        $numberOfCcjs,
        $totalProofsOfResidency,
        $dateOfBirthMatchOnInsight,
        $dateOfBirthMatchOnErAttainerDate,
        $homePhoneMatch,
        $totalProofsOfIdentity,
        $onBoe,
        $onPep,
        $onHalo,
        $onOns,
        $onOfac,
        $onCifas,
        $linkedForwardingAddress,
        $totalAlerts
    ){
        $this->currentlyOnEr = $currentlyOnEr;
        $this->numberOfOpenInsightLenders = $numberOfOpenInsightLenders;
        $this->currentlyBtListed = $currentlyBtListed;
        $this->numberOfCcjs = $numberOfCcjs;
        $this->totalProofsOfResidency = $totalProofsOfResidency;
        $this->dateOfBirthMatchOnInsight = $dateOfBirthMatchOnInsight;
        $this->dateOfBirthMatchOnErAttainerDate = $dateOfBirthMatchOnErAttainerDate;
        $this->homePhoneMatch = $homePhoneMatch;
        $this->totalProofsOfIdentity = $totalProofsOfIdentity;
        $this->onBoe = $onBoe;
        $this->onPep = $onPep;
        $this->onHalo = $onHalo;
        $this->onOns = $onOns;
        $this->onOfac = $onOfac;
        $this->onCifas = $onCifas;
        $this->linkedForwardingAddress = $linkedForwardingAddress;
        $this->totalAlerts = $totalAlerts;
    }
}