<?php

namespace DealTrak\Model\Rosetta\Personal;

use DealTrak\Model\Rosetta\EmploymentSection;
use DealTrak\Model\Rosetta\ResidenceSection;
use JMS\Serializer\Annotation as JMS;

/**
 * Class ApplicantSection
 *
 * @package DealTrak\Model\Rosetta\Personal
 */
class ApplicantSection
{
    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\XmlAttribute
     * @JMS\Groups({"personal"})
     */
    public $sequence;

    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\XmlAttribute
     * @JMS\Groups({"personal"})
     */
    public $type;

    /**
     * @var bool
     * @JMS\Type("bool")
     * @JMS\XmlAttribute
     * @JMS\SkipWhenEmpty
     * @JMS\Groups({"personal"})
     */
    public $soleTrader;

    /**
     * @var ApplicantDetailsSection
     * @JMS\Type("DealTrak\Model\Rosetta\Personal\ApplicantDetailsSection")
     * @JMS\Groups({"personal"})
     */
    public $detailsSection;

    /**
     * @var ResidenceSection[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\ResidenceSection>")
     * @JMS\XmlList(inline=true, entry="residence_section")
     * @JMS\Groups({"personal"})
     */
    public $residenceSection;

    /**
     * @var EmploymentSection[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\EmploymentSection>")
     * @JMS\XmlList(inline=true, entry="employment_section")
     * @JMS\Groups({"personal"})
     */
    public $employmentSection;

    /**
     * @var AffordabilitySection
     * @JMS\Type("DealTrak\Model\Rosetta\Personal\AffordabilitySection")
     * @JMS\SkipWhenEmpty
     * @JMS\Groups({"personal"})
     */
    public $affordabilitySection;

    /**
     * @var BankSection
     * @JMS\Type("DealTrak\Model\Rosetta\Personal\BankSection")
     * @JMS\Groups({"personal"})
     */
    public $bankSection;

    /**
     * @param int                     $sequence
     * @param int                     $type
     * @param bool|null               $soleTrader
     * @param ApplicantDetailsSection $detailsSection
     * @param ResidenceSection[]      $residenceSection
     * @param EmploymentSection[]     $employmentSection
     * @param AffordabilitySection    $affordabilitySection
     * @param BankSection             $bankSection
     */
    public function __construct(
        int $sequence,
        int $type,
        ApplicantDetailsSection $detailsSection,
        array $residenceSection,
        array $employmentSection,
        ?bool $soleTrader = null,
        ?AffordabilitySection $affordabilitySection = null,
        ?BankSection $bankSection = null
    ) {
        $this->sequence = $sequence;
        $this->type = $type;
        $this->soleTrader = $soleTrader;
        $this->detailsSection = $detailsSection;
        $this->residenceSection = $residenceSection;
        $this->employmentSection = $employmentSection;
        $this->affordabilitySection = $affordabilitySection;
        $this->bankSection = $bankSection;
    }

    /**
     * @return ApplicantDetailsSection
     */
    public function details()
    {
        return $this->detailsSection;
    }

    /**
     * @return array|EmploymentSection[]
     */
    public function employments()
    {
        return $this->employmentSection;
    }

    /**
     * @param EmploymentSection $employmentSection
     */
    public function addEmploymentSection(EmploymentSection $employmentSection)
    {
        $this->employmentSection[] = $employmentSection;
    }

    /**
     * @return EmploymentSection|mixed
     */
    public function firstEmployment()
    {
        return $this->employmentSection[0];
    }

    /**
     * @return array|ResidenceSection[]
     */
    public function residences()
    {
        return $this->residenceSection;
    }

    /**
     * @param ResidenceSection $residenceSection
     */
    public function addResidenceSection(ResidenceSection $residenceSection)
    {
        $this->residenceSection[] = $residenceSection;
    }

    /**
     * @return ResidenceSection
     */
    public function firstResidence()
    {
        return $this->residenceSection[0];
    }

    /**
     * @return AffordabilitySection
     */
    public function affordability()
    {
        return $this->affordabilitySection;
    }

    /**
     * @return BankSection
     */
    public function bank()
    {
        return $this->bankSection;
    }
}
