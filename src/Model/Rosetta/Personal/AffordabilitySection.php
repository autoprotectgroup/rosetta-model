<?php

namespace DealTrak\Model\Rosetta\Personal;

use JMS\Serializer\Annotation as JMS;

/**
 * Class AffordabilitySection
 *
 * @package DealTrak\Model\Rosetta\Personal
 */
class AffordabilitySection
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $replacementLoan;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $sustainability;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $monthlyMortgage;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $otherExpenditure;

    /**
     * @param string $replacementLoan
     * @param string $sustainability
     * @param string $monthlyMortgage
     * @param string $otherExpenditure
     */
    public function __construct(
        string $replacementLoan,
        string $sustainability,
        string $monthlyMortgage,
        string $otherExpenditure
    ) {
        $this->replacementLoan = $replacementLoan;
        $this->sustainability = $sustainability;
        $this->monthlyMortgage = $monthlyMortgage;
        $this->otherExpenditure = $otherExpenditure;
    }
}