<?php

namespace DealTrak\Model\Rosetta\Personal;

use JMS\Serializer\Annotation as JMS;

/**
 * Class BankSection
 *
 * @package DealTrak\Model\Rosetta\Personal
 */
class BankSection
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $accountName;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $accountNumber;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("sortcode")
     * @JMS\Groups({"personal"})
     */
    public $sortCode;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("bankname")
     * @JMS\Groups({"personal"})
     */
    public $bankName;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $branch;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $address;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $years;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $months;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $amex;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $chequeCard;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $dinersClub;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $masterCard;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $visa;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $debitCard;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $other;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $directDebit;

    /**
     * BankSection constructor.
     *
     * @param string $accountName
     * @param string $accountNumber
     * @param string $sortCode
     * @param string $bankName
     * @param string $branch
     * @param string $address
     * @param int    $months
     * @param int    $amex
     * @param int    $years
     * @param int    $chequeCard
     * @param int    $dinersClub
     * @param int    $masterCard
     * @param int    $visa
     * @param int    $debitCard
     * @param int    $other
     * @param int    $directDebit
     */
    public function __construct(
        ?string $accountName,
        string $accountNumber,
        ?string $sortCode,
        ?string $bankName,
        ?string $branch,
        string $address,
        int $years,
        int $months,
        int $amex = 0,
        int $chequeCard = 0,
        int $dinersClub = 0,
        int $masterCard = 0,
        int $visa = 0,
        int $debitCard = 0,
        int $other = 0,
        int $directDebit = 0
    ) {
        $this->accountName = $accountName;
        $this->accountNumber = $accountNumber;
        $this->sortCode = $sortCode;
        $this->bankName = $bankName;
        $this->branch = $branch;
        $this->address = $address;
        $this->years = $years;
        $this->months = $months;
        $this->amex = $amex;
        $this->chequeCard = $chequeCard;
        $this->dinersClub = $dinersClub;
        $this->masterCard = $masterCard;
        $this->visa = $visa;
        $this->debitCard = $debitCard;
        $this->other = $other;
        $this->directDebit = $directDebit;
    }
}