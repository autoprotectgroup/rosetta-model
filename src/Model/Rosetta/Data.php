<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Data
 *
 * @package DealTrak\Model\Rosetta\Base
 */
class Data
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $field;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $value;

    /**
     * @param string $field
     * @param string $value
     */
    public function __construct(string $field, string $value = '')
    {
        $this->field = $field;
        $this->value = $value;
    }
}