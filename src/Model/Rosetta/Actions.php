<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Actions
 *
 * @package DealTrak\Model\Rosetta
 */
class Actions
{
    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $taskId;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $taskStatusId;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $taskCreated;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $destinationStatusId;

    /**
     * @param int    $taskId
     * @param int    $taskStatusId
     * @param string $taskCreated
     * @param int    $destinationStatusId
     */
    public function __construct(
        int $taskId,
        int $taskStatusId,
        string $taskCreated,
        int $destinationStatusId
    ) {
        $this->taskId = $taskId;
        $this->taskStatusId = $taskStatusId;
        $this->taskCreated = $taskCreated;
        $this->destinationStatusId = $destinationStatusId;
    }
}