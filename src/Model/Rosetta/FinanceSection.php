<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class FinanceSection
 *
 * It may seem odd to you that data types for things like deposit and apr_rate are string rather than float,
 * it's because sprintf gets used on things before getting passed into this model, so it's a string.
 * I'm not saying that's right, but it's what happens so I went with it.
 *
 * It's probably going to be a good idea later to have the preSerialize hook sanitise this
 * (i.e. do the formatting in that rather than the callee doing it).
 *
 * Ah, the quirks of legacy.
 *
 * @package DealTrak\Model\Rosetta
 */
class FinanceSection
{
    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $financeType;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $vatQualifying;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $interestMethod;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $acceptanceFee;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $acceptanceMethod;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $acceptanceFeeInterestCharged;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $closureFee;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $closureMethod;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $closureFeeInterestCharged;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $vehicle;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $vehicleVat;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $options;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $optionsVat;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $accessories;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $accessoriesVat;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $delivery;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $deliveryVat;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $guarantee;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $guaranteeVat;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $rfl;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $rflTerm;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $regFee;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $fhbr;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $rob;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $profile1;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $profile2;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $profile3;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $profile1Payment;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $profile2Payment;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $profile3Payment;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $paymentFrequency;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $monthlyPayment = '';

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $regDealType;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $optedOut;

    /**
     * @var OptionsSection
     * @JMS\Type("DealTrak\Model\Rosetta\OptionsSection")
     * @JMS\Groups({"personal", "business"})
     */
    public $optionsSection;

    /**
     * @var AccessoriesSection
     * @JMS\Type("DealTrak\Model\Rosetta\AccessoriesSection")
     * @JMS\Groups({"personal", "business"})
     */
    public $accessoriesSection;

    /**
     * @var Vaps
     * @JMS\Type("DealTrak\Model\Rosetta\Vaps")
     * @JMS\Groups({"personal", "business"})
     */
    public $vaps;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $partEx;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $settlement;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $settlementLender;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $deposit;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $depositSterling;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $incomePromiseScheme;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $fda = null;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $rateType;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $flatRate;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $aprRate;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $term;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $pauseType = '';

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $balloon;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $annualMileage = null;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $vatRate;

    /**
     * @var Cpi
     * @JMS\Type("DealTrak\Model\Rosetta\Cpi")
     * @JMS\SkipWhenEmpty
     * @JMS\Groups({"personal", "business"})
     */
    public $cpi;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     * @JMS\SkipWhenEmpty
     */
    public $financePackage = '';

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $financePackageId = '';

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $ltv;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $ltvGlass;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $creditScore = '';

    /**
     * @param string                  $financeType
     * @param int                     $vatQualifying
     * @param string                  $interestMethod
     * @param string                  $acceptanceFee
     * @param string                  $acceptanceMethod
     * @param string                  $acceptanceFeeInterestCharged
     * @param string                  $closureFee
     * @param string                  $closureMethod
     * @param string                  $closureFeeInterestCharged
     * @param string                  $vehicle
     * @param string                  $vehicleVat
     * @param string                  $options
     * @param string                  $optionsVat
     * @param string                  $accessories
     * @param string                  $accessoriesVat
     * @param string                  $delivery
     * @param string                  $deliveryVat
     * @param string                  $guarantee
     * @param string                  $guaranteeVat
     * @param string                  $rfl
     * @param int                     $rflTerm
     * @param string                  $regFee
     * @param string                  $fhbr
     * @param string                  $rob
     * @param string                  $profile1
     * @param string                  $profile2
     * @param string                  $profile3
     * @param string                  $profile1Payment
     * @param string                  $profile2Payment
     * @param string                  $profile3Payment
     * @param string                  $monthlyPayment
     * @param string                  $regDealType
     * @param int                     $optedOut
     * @param OptionsSection|null     $optionsSection
     * @param AccessoriesSection|null $accessoriesSection
     * @param vaps|null               $vaps
     * @param string                  $partEx
     * @param string                  $settlement
     * @param string|null             $settlementLender
     * @param string                  $deposit
     * @param string                  $depositSterling
     * @param int                     $incomePromiseScheme
     * @param float|null              $fda
     * @param string                  $rateType
     * @param string                  $flatRate
     * @param string                  $aprRate
     * @param int                     $term
     * @param int                     $paymentFrequency
     * @param string|null             $pauseType
     * @param string                  $balloon
     * @param int|null                $annualMileage
     * @param string                  $vatRate
     * @param Cpi|null                $cpi
     * @param string|null             $financePackage
     * @param string|null             $financePackageId
     * @param string                  $ltv
     * @param string                  $ltvGlass
     * @param string|null             $creditScore
     */
    public function __construct(
        string $financeType,
        int $vatQualifying,
        string $interestMethod,
        string $acceptanceFee,
        string $acceptanceMethod,
        string $acceptanceFeeInterestCharged,
        string $closureFee,
        string $closureMethod,
        string $closureFeeInterestCharged,
        string $vehicle,
        string $vehicleVat,
        string $options,
        string $optionsVat,
        string $accessories,
        string $accessoriesVat,
        string $delivery,
        string $deliveryVat,
        string $guarantee,
        string $guaranteeVat,
        string $rfl,
        int $rflTerm,
        string $regFee,
        string $fhbr,
        string $rob,
        string $profile1,
        string $profile2,
        string $profile3,
        string $profile1Payment,
        string $profile2Payment,
        string $profile3Payment,
        string $monthlyPayment,
        string $regDealType,
        int $optedOut,
        string $partEx,
        string $settlement,
        string $deposit,
        string $depositSterling,
        int $incomePromiseScheme,
        string $rateType,
        string $flatRate,
        string $aprRate,
        int $term,
        int $paymentFrequency,
        string $balloon,
        string $vatRate,
        string $ltv,
        string $ltvGlass,
        ?OptionsSection $optionsSection = null,
        ?AccessoriesSection $accessoriesSection = null,
        ?vaps $vaps = null,
        ?string $settlementLender = null,
        ?float $fda = null,
        ?string $pauseType = null,
        ?int $annualMileage = null,
        ?Cpi $cpi = null,
        ?string $financePackage = null,
        ?string $financePackageId = null,
        ?string $creditScore = null
    ) {
        $this->financeType = $financeType;
        $this->vatQualifying = $vatQualifying;
        $this->interestMethod = $interestMethod;
        $this->acceptanceFee = $acceptanceFee;
        $this->acceptanceMethod = $acceptanceMethod;
        $this->acceptanceFeeInterestCharged = $acceptanceFeeInterestCharged;
        $this->closureFee = $closureFee;
        $this->closureMethod = $closureMethod;
        $this->closureFeeInterestCharged = $closureFeeInterestCharged;
        $this->vehicle = $vehicle;
        $this->vehicleVat = $vehicleVat;
        $this->options = $options;
        $this->optionsVat = $optionsVat;
        $this->accessories = $accessories;
        $this->accessoriesVat = $accessoriesVat;
        $this->delivery = $delivery;
        $this->deliveryVat = $deliveryVat;
        $this->guarantee = $guarantee;
        $this->guaranteeVat = $guaranteeVat;
        $this->rfl = $rfl;
        $this->rflTerm = $rflTerm;
        $this->regFee = $regFee;
        $this->fhbr = $fhbr;
        $this->rob = $rob;
        $this->profile1 = $profile1;
        $this->profile2 = $profile2;
        $this->profile3 = $profile3;
        $this->profile1Payment = $profile1Payment;
        $this->profile2Payment = $profile2Payment;
        $this->profile3Payment = $profile3Payment;
        $this->monthlyPayment = $monthlyPayment;
        $this->regDealType = $regDealType;
        $this->optedOut = $optedOut;
        $this->vaps = $vaps;
        $this->partEx = $partEx;
        $this->settlement = $settlement;
        $this->deposit = $deposit;
        $this->depositSterling = $depositSterling;
        $this->incomePromiseScheme = $incomePromiseScheme;
        $this->fda = $fda;
        $this->rateType = $rateType;
        $this->flatRate = $flatRate;
        $this->aprRate = $aprRate;
        $this->term = $term;
        $this->paymentFrequency = $paymentFrequency;
        $this->pauseType = $pauseType;
        $this->balloon = $balloon;
        $this->annualMileage = $annualMileage;
        $this->vatRate = $vatRate;
        $this->financePackage = $financePackage;
        $this->financePackageId = $financePackageId;
        $this->ltv = $ltv;
        $this->ltvGlass = $ltvGlass;
        $this->creditScore = $creditScore;
        $this->optionsSection = $optionsSection;
        $this->accessoriesSection = $accessoriesSection;
        $this->settlementLender = $settlementLender;
        $this->cpi = $cpi;
    }

    /**
     * Return a list of vaps.
     *
     * @return array|Vap[]
     */
    public function vaps()
    {
        return $this->vaps->vaps;
    }

    /**
     * @JMS\PreSerialize
     */
    public function onPreSerialize()
    {
        $this->annualMileage = $this->annualMileage === null ? '' : $this->annualMileage;
    }
}
