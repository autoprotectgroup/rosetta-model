<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class ExtrasSection
 *
 * @package DealTrak\Model\Rosetta
 */
class ExtrasSection
{
    /**
     * @var Notes
     * @JMS\Type("DealTrak\Model\Rosetta\Notes")
     * @JMS\Groups({"personal", "business"})
     */
    public $notes;

    /**
     * @var LenderNotes
     * @JMS\Type("DealTrak\Model\Rosetta\LenderNotes")
     * @JMS\SkipWhenEmpty
     * @JMS\Groups({"personal", "business"})
     */
    public $lenderNotes;

    /**
     * @var Data[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\Data>")
     * @JMS\XmlList(inline=true, entry="data")
     * @JMS\Groups({"personal", "business"})
     */
    public $data;

    /**
     * @param Notes            $notes
     * @param LenderNotes|null $lenderNotes
     * @param Data[]           $data
     */
    public function __construct(
        Notes $notes = null,
        LenderNotes $lenderNotes = null,
        array $data = []
    ) {
        $this->notes = $notes;
        $this->lenderNotes = $lenderNotes;
        $this->data = $data;
    }
}