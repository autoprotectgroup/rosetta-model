<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class ResidenceSection
 *
 * @package DealTrak\Model\Rosetta
 */
class ResidenceSection
{
    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\XmlAttribute
     * @JMS\Groups({"personal", "business"})
     */
    public $sequence;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $flat;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $houseName;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $houseNumber;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $street;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $district;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("towncity")
     * @JMS\Groups({"personal", "business"})
     */
    public $townCity;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $county;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $postcode;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $years;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $months;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $residentialStatus;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $ukAddress;

    /**
     * @param int    $sequence
     * @param string $flat
     * @param string $houseName
     * @param string $houseNumber
     * @param string $street
     * @param string $district
     * @param string $townCity
     * @param string $county
     * @param string $postcode
     * @param int $years
     * @param int $months
     * @param int $residentialStatus
     * @param int $ukAddress
     */
    public function __construct(
        int $sequence,
        string $street,
        string $townCity,
        string $county,
        string $postcode,
        int $years,
        int $months,
        int $residentialStatus,
        int $ukAddress,
        ?string $flat = '',
        ?string $houseName = '',
        ?string $houseNumber = '',
        ?string $district = ''
    ) {
        $this->sequence = $sequence;
        $this->flat = $flat;
        $this->houseName = $houseName;
        $this->houseNumber = $houseNumber;
        $this->street = $street;
        $this->district = $district;
        $this->townCity = $townCity;
        $this->county = $county;
        $this->postcode = $postcode;
        $this->years = $years;
        $this->months = $months;
        $this->residentialStatus = $residentialStatus;
        $this->ukAddress = $ukAddress;
    }
}