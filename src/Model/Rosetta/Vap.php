<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Vap
 *
 * @package DealTrak\Model\Rosetta
 */
class Vap
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $productId;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $supplierId;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $productCode;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $product;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $premium;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal"})
     */
    public $ipt;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $vapTerm;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $taxType;

    /**
     * @param string $productId
     * @param string $supplierId
     * @param string $productCode
     * @param string $product
     * @param string  $premium
     * @param string  $ipt
     * @param int    $vapTerm
     * @param string $taxType
     */
    public function __construct(
        ?string $productId,
        ?string $supplierId,
        ?string $productCode,
        ?string $product,
        ?string $premium,
        ?string $ipt,
        ?int $vapTerm,
        ?string $taxType
    ) {
        $this->productId = $productId;
        $this->supplierId = $supplierId;
        $this->productCode = $productCode;
        $this->product = $product;
        $this->premium = $premium;
        $this->ipt = $ipt;
        $this->vapTerm = $vapTerm;
        $this->taxType = $taxType;
    }
}