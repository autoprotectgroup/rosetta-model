<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class OptionsSection
 *
 * @package DealTrak\Model\Rosetta
 */
class OptionsSection
{
    /**
     * @var array|Option[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\Option>")
     * @JMS\XmlList(inline=true, entry="option")
     * @JMS\Groups({"personal", "business"})
     */
    public $options;

    /**
     * @param array|Option[] $options
     */
    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * @param Option $option
     */
    public function addOption(Option $option)
    {
        $this->options[] = $option;
    }
}