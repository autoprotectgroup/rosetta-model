<?php

namespace DealTrak\Model\Rosetta\Business;

use Fsuk\RosettaBundle\Traits\RosettaModelDateTransformerTrait;
use JMS\Serializer\Annotation as JMS;

/**
 * Class OrganisationDetails
 *
 * @package DealTrak\Model\Rosetta\Business
 */
class OrganisationDetails
{
    use RosettaModelDateTransformerTrait;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $organisationName;

    /**
     * @var null|string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SkipWhenEmpty
     * @JMS\Groups({"business"})
     */
    public $previousOrganisationName;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $organisationType;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $contactName;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $mainPhone;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $email;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $dateEstablished;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $companyRegistration;

    /**
     * @var null|string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $vatNumber;

    /**
     * Note this is the SIC code, and is 5 chars in length including zeros as necessary.
     * @see http://resources.companieshouse.gov.uk/sic/
     *
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $industryTypeId;

    /**
     * @param string      $organisationName
     * @param null|string $previousOrganisationName
     * @param int         $organisationType
     * @param string      $contactName
     * @param string      $mainPhone
     * @param string      $email
     * @param string      $dateEstablished
     * @param string      $companyRegistration
     * @param null|string $vatNumber
     * @param int         $industryTypeId
     */
    public function __construct(
        string $organisationName,
        ?string $previousOrganisationName = null,
        int $organisationType,
        string $contactName,
        string $mainPhone,
        string $email,
        string $dateEstablished,
        ?string $companyRegistration = null,
        ?string $vatNumber = null,
        ?string $industryTypeId = null
    ) {
        $this->organisationName = $organisationName;
        $this->previousOrganisationName = $previousOrganisationName;
        $this->organisationType = $organisationType;
        $this->contactName = $contactName;
        $this->mainPhone = $mainPhone;
        $this->email = $email;
        $this->dateEstablished = $dateEstablished;
        $this->companyRegistration = $companyRegistration;
        $this->vatNumber = $vatNumber;
        $this->industryTypeId = $industryTypeId;
    }

    /**
     * @JMS\PreSerialize
     */
    public function onPreSerialize()
    {
        $this->industryTypeId = $this->industryTypeId === 0 || $this->industryTypeId === null ? '' : $this->industryTypeId;
    }
}