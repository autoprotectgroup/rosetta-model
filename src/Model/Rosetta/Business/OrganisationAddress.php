<?php

namespace DealTrak\Model\Rosetta\Business;

use JMS\Serializer\Annotation as JMS;

/**
 * Class OrganisationAddress
 *
 * @package DealTrak\Model\Rosetta\Business
 */
class OrganisationAddress
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $building;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("subbuilding")
     * @JMS\Groups({"business"})
     */
    public $subBuilding = '';

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $street = '';

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $district = '';

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("towncity")
     * @JMS\Groups({"business"})
     */
    public $townCity;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $county = '';

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $postcode;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $years;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $months;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $businessTenure;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $ukAddress;

    /**
     * @param string $building
     * @param string $subBuilding
     * @param string $street
     * @param string $district
     * @param string $townCity
     * @param string $county
     * @param string $postcode
     * @param int    $years
     * @param int    $months
     * @param int    $businessTenure
     * @param int    $ukAddress
     */
    public function __construct(
        string $building,
        ?string $subBuilding = '',
        ?string $street = '',
        ?string $district = '',
        ?string $townCity,
        ?string $county = '',
        string $postcode,
        int $years,
        int $months,
        int $businessTenure,
        int $ukAddress
    ) {
        $this->building = $building;
        $this->subBuilding = $subBuilding;
        $this->street = $street;
        $this->district = $district;
        $this->townCity = $townCity;
        $this->county = $county;
        $this->postcode = $postcode;
        $this->years = $years;
        $this->months = $months;
        $this->businessTenure = $businessTenure;
        $this->ukAddress = $ukAddress;
    }
}