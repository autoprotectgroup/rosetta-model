<?php

namespace DealTrak\Model\Rosetta\Business;

use DealTrak\Model\Rosetta\ExtrasSection;
use DealTrak\Model\Rosetta\FinanceSection;
use DealTrak\Model\Rosetta\NoVehicleYetSection;
use DealTrak\Model\Rosetta\VehicleSection;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Business
 *
 * @package DealTrak\Model\Rosetta\Business
 */
class Business
{
    /**
     * @var OrganisationSection
     * @JMS\Type("DealTrak\Model\Rosetta\Business\OrganisationSection")
     * @JMS\Groups({"business"})
     */
    public $organisationSection;

    /**
     * @var DirectorSection
     * @JMS\Type("DealTrak\Model\Rosetta\Business\DirectorSection")
     * @JMS\Groups({"business"})
     */
    public $directorSection;

    /**
     * @var VehicleSection
     * @JMS\Type("DealTrak\Model\Rosetta\VehicleSection")
     * @JMS\Groups({"business"})
     */
    public $vehicleSection;

    /**
     * @var NoVehicleYetSection
     * @JMS\Type("DealTrak\Model\Rosetta\NoVehicleYetSection")
     * @JMS\SerializedName("vehicle_section")
     * @JMS\Groups({"business"})
     */
    public $noVehicleYetSection;

    /**
     * @var FinanceSection
     * @JMS\Type("DealTrak\Model\Rosetta\FinanceSection")
     * @JMS\Groups({"business"})
     */
    public $financeSection;

    /**
     * @var ExtrasSection
     * @JMS\Type("DealTrak\Model\Rosetta\ExtrasSection")
     * @JMS\Groups({"business"})
     */
    public $extrasSection;

    /**
     * @param OrganisationSection      $organisationSection
     * @param DirectorSection          $directorSection
     * @param VehicleSection|null      $vehicleSection
     * @param NoVehicleYetSection|null $noVehicleYetSection
     * @param FinanceSection           $financeSection
     * @param ExtrasSection            $extrasSection
     */
    public function __construct(
        OrganisationSection $organisationSection,
        DirectorSection $directorSection,
        ?VehicleSection $vehicleSection,
        ?NoVehicleYetSection $noVehicleYetSection = null,
        FinanceSection $financeSection,
        ExtrasSection $extrasSection
    ) {
        if ($vehicleSection !== null && $noVehicleYetSection !== null) {
            throw new \LogicException('Only one type of vehicle section can exist');
        }

        $this->organisationSection = $organisationSection;
        $this->directorSection = $directorSection;
        $this->vehicleSection = $vehicleSection;
        $this->noVehicleYetSection = $noVehicleYetSection;
        $this->financeSection = $financeSection;
        $this->extrasSection = $extrasSection;
    }

    /**
     * @return OrganisationSection
     */
    public function organisation()
    {
        return $this->organisationSection;
    }

    /**
     * @return Director[]
     */
    public function directors()
    {
        return $this->directorSection->directors();
    }

    /**
     * @return VehicleSection
     */
    public function vehicle()
    {
        return $this->vehicleSection;
    }

    /**
     * @return FinanceSection
     */
    public function finance()
    {
        return $this->financeSection;
    }

    /**
     * @return ExtrasSection
     */
    public function extras()
    {
        return $this->extrasSection;
    }
}