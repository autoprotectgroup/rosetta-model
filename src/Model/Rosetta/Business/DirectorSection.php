<?php

namespace DealTrak\Model\Rosetta\Business;

use JMS\Serializer\Annotation as JMS;

/**
 * Class DirectorSection
 *
 * @package DealTrak\Model\Rosetta\Business
 */
class DirectorSection
{
    /**
     * @var Director[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\Business\Director>")
     * @JMS\XmlList(inline=true, entry="director")
     * @JMS\Groups({"business"})
     */
    public $director;

    /**
     * @param array|Director[] $director
     */
    public function __construct(array $director)
    {
        $this->director = $director;
    }

    /**
     * @return array|Director[]
     */
    public function directors()
    {
        return $this->director;
    }

    /**
     * @return Director
     */
    public function firstDirector()
    {
        return $this->director[0];
    }
}