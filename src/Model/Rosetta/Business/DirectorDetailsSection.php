<?php

namespace DealTrak\Model\Rosetta\Business;

use JMS\Serializer\Annotation as JMS;

/**
 * Class DirectorDetailsSection
 *
 * @package DealTrak\Model\Rosetta\Business
 */
class DirectorDetailsSection
{
    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $title;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $forename;

    /**
     * @var null|string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $middlename;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $surname;

    /**
     * @var int|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $relationship;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $dob;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $gender;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $maritalStatus;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $drivingLicense;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $ukResident;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $nationality;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $homePhone;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $mobile;

    /**
     * @var int|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $pension;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $distanceMarketed;

    /**
     * @param int $title
     * @param string $forename
     * @param string $middlename
     * @param string $surname
     * @param int|null $relationship
     * @param string $dob
     * @param int $gender
     * @param int $maritalStatus
     * @param int|null $drivingLicense
     * @param int $ukResident
     * @param int $nationality
     * @param string $homePhone
     * @param string $mobile
     * @param int|null $pension
     * @param int $distanceMarketed
     */
    public function __construct(
        int $title,
        string $forename,
        ?string $middlename = null,
        string $surname,
        ?int $relationship,
        ?string $dob,
        int $gender,
        int $maritalStatus,
        ?int $drivingLicense = null,
        int $ukResident,
        int $nationality,
        string $homePhone,
        string $mobile,
        ?int $pension,
        int $distanceMarketed
    ) {
        $this->title = $title;
        $this->forename = $forename;
        $this->middlename = $middlename;
        $this->surname = $surname;
        $this->relationship = $relationship;
        $this->dob = $dob;
        $this->gender = $gender;
        $this->maritalStatus = $maritalStatus;
        $this->drivingLicense = $drivingLicense;
        $this->ukResident = $ukResident;
        $this->nationality = $nationality;
        $this->homePhone = $homePhone;
        $this->mobile = $mobile;
        $this->pension = $pension;
        $this->distanceMarketed = $distanceMarketed;
    }
}