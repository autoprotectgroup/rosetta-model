<?php

namespace DealTrak\Model\Rosetta\Business;

use DealTrak\Model\Rosetta\EmploymentSection;
use DealTrak\Model\Rosetta\ResidenceSection;
use JMS\Serializer\Annotation as JMS;

/**
 * Class Director
 *
 * @package DealTrak\Model\Rosetta\Business
 */
class Director
{
    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\XmlAttribute
     * @JMS\Groups({"business"})
     */
    public $sequence;

    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\XmlAttribute
     * @JMS\Groups({"business"})
     */
    public $type;

    /**
     * @var DirectorDetailsSection
     * @JMS\Type("DealTrak\Model\Rosetta\Business\DirectorDetailsSection")
     * @JMS\Groups({"business"})
     */
    public $detailsSection;

    /**
     * @var ResidenceSection[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\ResidenceSection>")
     * @JMS\XmlList(inline=true, entry="residence_section")
     * @JMS\Groups({"business"})
     */
    public $residenceSection;

    /**
     * @var EmploymentSection[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\EmploymentSection>")
     * @JMS\XmlList(inline=true, entry="employment_section")
     * @JMS\Groups({"business"})
     */
    public $employmentSection;

    /**
     * @param int                    $sequence
     * @param int                    $type
     * @param DirectorDetailsSection $detailsSection
     * @param ResidenceSection[]     $residenceSection
     * @param EmploymentSection[]    $employmentSection
     */
    public function __construct(
        int $sequence,
        int $type,
        DirectorDetailsSection $detailsSection,
        array $residenceSection,
        array $employmentSection
    ) {
        $this->detailsSection = $detailsSection;
        $this->residenceSection = $residenceSection;
        $this->employmentSection = $employmentSection;
        $this->sequence = $sequence;
        $this->type = $type;
    }

    /**
     * @return DirectorDetailsSection
     */
    public function details()
    {
        return $this->detailsSection;
    }

    /**
     * @return array|EmploymentSection[]
     */
    public function employments()
    {
        return $this->employmentSection;
    }

    /**
     * @return EmploymentSection
     */
    public function firstEmployment()
    {
        return $this->employmentSection[0];
    }

    /**
     * @return array|ResidenceSection[]
     */
    public function residences()
    {
        return $this->residenceSection;
    }

    /**
     * @return ResidenceSection
     */
    public function firstResidence()
    {
        return $this->residenceSection[0];
    }
}