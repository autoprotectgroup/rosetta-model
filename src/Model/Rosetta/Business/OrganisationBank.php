<?php

namespace DealTrak\Model\Rosetta\Business;

use JMS\Serializer\Annotation as JMS;

/**
 * Class OrganisationBank
 *
 * @package DealTrak\Model\Rosetta\Business
 */
class OrganisationBank
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $accountName;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $accountNumber;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("sortcode")
     * @JMS\Groups({"business"})
     */
    public $sortCode;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("bankname")
     * @JMS\Groups({"business"})
     */
    public $bankName;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $branch;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $address;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $years;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $months;

    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"business"})
     */
    public $directDebit;

    /**
     * @param string $accountName
     * @param string $accountNumber
     * @param string $sortCode
     * @param string $bankName
     * @param string $branch
     * @param string $address
     * @param int    $years
     * @param int    $months
     * @param int    $directDebit
     */
    public function __construct(
        string $accountName,
        string $accountNumber,
        string $sortCode,
        string $bankName,
        string $branch,
        string $address,
        int $years,
        int $months,
        int $directDebit = 0
    ) {

        $this->accountName = $accountName;
        $this->accountNumber = $accountNumber;
        $this->sortCode = $sortCode;
        $this->bankName = $bankName;
        $this->branch = $branch;
        $this->address = $address;
        $this->years = $years;
        $this->months = $months;
        $this->directDebit = $directDebit;
    }
}