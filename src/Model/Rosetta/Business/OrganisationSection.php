<?php

namespace DealTrak\Model\Rosetta\Business;

use JMS\Serializer\Annotation as JMS;

/**
 * Class OrganisationSection
 *
 * @package DealTrak\Model\Rosetta\Business
 */
class OrganisationSection
{
    /**
     * @var OrganisationDetails
     * @JMS\Type("DealTrak\Model\Rosetta\Business\OrganisationDetails")
     * @JMS\Groups({"business"})
     */
    public $organisationDetails;

    /**
     * @var OrganisationAddress
     * @JMS\Type("DealTrak\Model\Rosetta\Business\OrganisationAddress")
     * @JMS\Groups({"business"})
     */
    public $organisationAddress;

    /**
     * @var RegisteredAddress
     * @JMS\Type("DealTrak\Model\Rosetta\Business\RegisteredAddress")
     * @JMS\Groups({"business"})
     */
    public $registeredAddress;

    /**
     * @var OrganisationBank
     * @JMS\Type("DealTrak\Model\Rosetta\Business\OrganisationBank")
     * @JMS\Groups({"business"})
     */
    public $organisationBank;

    /**
     * @param OrganisationDetails $organisationDetails
     * @param OrganisationAddress $organisationAddress
     * @param RegisteredAddress   $registeredAddress
     * @param OrganisationBank    $organisationBank
     */
    public function __construct(
        OrganisationDetails $organisationDetails,
        OrganisationAddress $organisationAddress,
        RegisteredAddress $registeredAddress,
        OrganisationBank $organisationBank
    ) {
        $this->organisationDetails = $organisationDetails;
        $this->organisationAddress = $organisationAddress;
        $this->registeredAddress = $registeredAddress;
        $this->organisationBank = $organisationBank;
    }

    /**
     * @return OrganisationDetails
     */
    public function details()
    {
        return $this->organisationDetails;
    }

    /**
     * @return OrganisationAddress
     */
    public function address()
    {
        return $this->organisationAddress;
    }

    /**
     * @return OrganisationBank
     */
    public function bank()
    {
        return $this->organisationBank;
    }
}