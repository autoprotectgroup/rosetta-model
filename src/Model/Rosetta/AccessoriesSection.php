<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class AccessoriesSection
 *
 * @package DealTrak\Model\Rosetta
 */
class AccessoriesSection
{
    /**
     * @var array|Accessory[]
     * @JMS\Type("array<DealTrak\Model\Rosetta\Accessory>")
     * @JMS\XmlList(inline=true, entry="accessory")
     * @JMS\Groups({"personal", "business"})
     */
    public $accessories;

    /**
     * @param array|Accessory[] $accessories
     */
    public function __construct(array $accessories = [])
    {
        $this->accessories = $accessories;
    }

    /**
     * @param Accessory $accessory
     */
    public function addAccessory(Accessory $accessory)
    {
        $this->accessories[] = $accessory;
    }
}