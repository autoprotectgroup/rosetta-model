<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class NoVehicleYetSection
 *
 * @package DealTrak\Model\Rosetta
 */
class NoVehicleYetSection
{
    /**
     * @var int
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $noVehicleYet;

    /**
     * @param int $noVehicleYet
     */
    public function __construct(int $noVehicleYet)
    {
        $this->noVehicleYet = $noVehicleYet;
    }
}