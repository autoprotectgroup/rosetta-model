<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Routing
 *
 * @package DealTrak\Model\Rosetta
 */
class Routing
{
    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\Groups({"personal", "business"})
     */
    public $destination;

    /**
     * @var int|null
     * @JMS\Type("int")
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerContactId;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerName;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerBuilding;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("broker_dealer_subbuilding")
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerSubBuilding;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerStreet;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerDistrict;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\SerializedName("broker_dealer_towncity")
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerTownCity;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerCounty;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerPostcode;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerStatus;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerEmail;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerPhone;

    /**
     * @var string|null
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"broker"})
     */
    public $brokerDealerFax;

    /**
     * @var int
     * @JMS\Type("int")
     * @JMS\Groups({"personal", "business"})
     */
    public $client;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $branchId;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $contactForename;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $contactSurname;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $contactPhone;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $contactEmail;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $identifier;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $username;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $password;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $clientReference;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $rosettaReference;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $sourceReference;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $proposalCreatedAt;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $createdAt;

    /**
     * @param int            $destination
     * @param int            $client
     * @param string         $branchId
     * @param string         $contactForename
     * @param string         $contactSurname
     * @param string         $contactPhone
     * @param string         $contactEmail
     * @param string         $identifier
     * @param string         $username
     * @param string         $password
     * @param string         $clientReference
     * @param string|null    $rosettaReference
     * @param string|null    $sourceReference
     * @param string|null    $proposalCreatedAt
     * @param string|null    $createdAt
     * @param int|null       $brokerDealerContactId
     * @param string|null    $brokerDealerName
     * @param string|null    $brokerDealerBuilding
     * @param string|null    $brokerDealerSubBuilding
     * @param string|null    $brokerDealerStreet
     * @param string|null    $brokerDealerDistrict
     * @param string|null    $brokerDealerTownCity
     * @param string|null    $brokerDealerCounty
     * @param string|null    $brokerDealerPostcode
     * @param string|null    $brokerDealerStatus
     * @param string|null    $brokerDealerEmail
     * @param string|null    $brokerDealerPhone
     * @param string|null    $brokerDealerFax
     */
    public function __construct(
        int $destination,
        int $client,
        string $branchId,
        string $contactForename,
        string $contactSurname,
        string $contactPhone,
        string $contactEmail,
        string $identifier,
        string $username,
        string $password,
        string $clientReference,

        ?string $proposalCreatedAt,
        ?string $createdAt,

        ?string $rosettaReference = '',
        ?string $sourceReference = '',
        ?int $brokerDealerContactId = null,
        ?string $brokerDealerName = '',
        ?string $brokerDealerBuilding = '',
        ?string $brokerDealerSubBuilding = '',
        ?string $brokerDealerStreet = '',
        ?string $brokerDealerDistrict = '',
        ?string $brokerDealerTownCity = '',
        ?string $brokerDealerCounty = '',
        ?string $brokerDealerPostcode = '',
        ?string $brokerDealerStatus = '',
        ?string $brokerDealerEmail = '',
        ?string $brokerDealerPhone = '',
        ?string $brokerDealerFax = ''
    ) {
        $this->destination = $destination;
        $this->client = $client;
        $this->branchId = $branchId;
        $this->contactForename = $contactForename;
        $this->contactSurname = $contactSurname;
        $this->contactPhone = $contactPhone;
        $this->contactEmail = $contactEmail;
        $this->identifier = $identifier;
        $this->username = $username;
        $this->password = $password;
        $this->clientReference = $clientReference;
        $this->rosettaReference = $rosettaReference;
        $this->sourceReference = $sourceReference;
        $this->proposalCreatedAt = $proposalCreatedAt;
        $this->createdAt = $createdAt;
        $this->brokerDealerContactId = $brokerDealerContactId;
        $this->brokerDealerName = $brokerDealerName;
        $this->brokerDealerBuilding = $brokerDealerBuilding;
        $this->brokerDealerSubBuilding = $brokerDealerSubBuilding;
        $this->brokerDealerStreet = $brokerDealerStreet;
        $this->brokerDealerDistrict = $brokerDealerDistrict;
        $this->brokerDealerTownCity = $brokerDealerTownCity;
        $this->brokerDealerCounty = $brokerDealerCounty;
        $this->brokerDealerPostcode = $brokerDealerPostcode;
        $this->brokerDealerStatus = $brokerDealerStatus;
        $this->brokerDealerEmail = $brokerDealerEmail;
        $this->brokerDealerPhone = $brokerDealerPhone;
        $this->brokerDealerFax = $brokerDealerFax;
    }
}