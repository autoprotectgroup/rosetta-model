<?php

namespace DealTrak\Model\Rosetta;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Option
 *
 * @package DealTrak\Model\Rosetta
 */
class Option
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $description;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $net;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata=false)
     * @JMS\Groups({"personal", "business"})
     */
    public $vat;

    /**
     * @param string $description
     * @param string  $net
     * @param string  $vat
     */
    public function __construct(
        string $description,
        string $net,
        string $vat
    ) {
        $this->description = $description;
        $this->net = $net;
        $this->vat = $vat;
    }
}