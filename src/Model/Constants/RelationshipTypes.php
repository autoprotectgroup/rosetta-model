<?php

namespace DealTrak\Model\Constants;

class RelationshipTypes
{
    const PARENT = 1;
    const SPOUSE = 2;
    const PARTNER = 3;
    const SIBLING = 4;
    const FRIEND = 5;
    const GRANDPARENT = 6;
    const CHILD = 7;
    const GRANDCHILD = 8;
    const OTHER = 9;
}
