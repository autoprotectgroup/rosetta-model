<?php

namespace DealTrak\Model\Constants;

class ReferralTypes
{
    const TYPE_NA = 1;
    const TYPE_A = 2;
    const TYPE_B = 3;
    const TYPE_C = 4;
}
