<?php

namespace DealTrak\Model\Constants;

class SourceTypes
{
    const DIRECT_INPUT = 1;
    const FOURSQUARE = 2;
    const LEAD_IMPORT = 3;
    const APPLICATION_FORMS = 4;
}
