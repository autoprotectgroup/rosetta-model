<?php

namespace DealTrak\Model\Constants;

class BusinessApplicantTypes
{
    const DIRECTOR = 1;
    const PARTNER = 2;
    const PROPRIETER = 3;
    const NON_EXEC = 4;
    const OTHER = 5;
    const CO_SEC = 6;
    const SOLE_TRADER = 7;
}