<?php

namespace DealTrak\Model\Constants;

class SalesTypes
{
    const RETAIL = 1;
    const FLEET = 2;
    const MOTABILITY = 3;
    const TRADE = 4;
    const COMMERCIAL = 5;
}
