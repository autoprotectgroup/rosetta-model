<?php

namespace DealTrak\Model\Constants;

class TransmissionTypes
{
    const MANUAL = 1;
    const AUTOMATIC = 2;
    const SEMI_AUTOMATIC = 3;
}
