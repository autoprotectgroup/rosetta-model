<?php

namespace DealTrak\Model\Constants;

class BodyStyles
{
    const HARDTOP = 1;
    const CABRIOLET = 2;
    const COUPE = 3;
    const ESTATE = 4;
    const FASTBACK = 5;
    const HATCHBACK = 6;
    const SOFT_TOP = 7;
    const SPEEDSTER = 8;
    const ROADSTER = 9;
    const SALOON = 10;
    const TOURER = 11;
    const CONVERTIBLE = 12;
    const STATION_WAGON = 13;
}
