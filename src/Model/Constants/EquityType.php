<?php

namespace DealTrak\Model\Constants;

class EquityType
{
    const GEAUTO_METHOD = 1;
    const BARCLAYS_METHOD = 2;
    const CREATION_METHOD = 3;
    const BLACKHORSE_METHOD = 4;
    const TFCD_METHOD = 5;
    const FORTIS_METHOD = 6;
    const HITACHI_METHOD = 7;
    const DUNCTON = 8;
}
