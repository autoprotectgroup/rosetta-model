<?php

namespace DealTrak\Model\Constants;

class DestinationTypes
{
    const MANUFACTURER = 1;
    const LENDER = 2;
    const BROKER = 3;
    const SPONSORED = 4;
    const LEAD = 5;
    const VAP_SUPPLIER = 6;
    const LEAD_OUT = 7;
}
