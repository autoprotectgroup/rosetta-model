<?php

namespace DealTrak\Model\Constants;

class DestinationStatus
{
    const NO_DECISION = 0;
    const UNKNOWN = 1;
    const APPROVED = 2;
    const COND_APPROVED = 3;
    const REFERRED = 4;
    const DECLINED = 5;
    const CANCELLED = 6;
    const NTU = 7;
    const ERROR = 8;
    const RECEIVED_OK = 9;
    const INFO_REQUIRED = 10;
    const DOCS_RECEIVED = 11;
    const PAID_OUT = 12;
    const RESUBMIT_REQUIRED = 13;
    const NOT_SENT = 14;
    const TIMEOUT = 15;
    const CONTACT_UNDERWRITER = 16;
    const CCA_CANCELLED = 17;
    const CUSTOMER_CANCELLED = 18;
    const THIRD_PARTY_CANCELLED = 19;
    const UNWOUND = 20;
    const DOCUMENT_ERROR = 21;
}
