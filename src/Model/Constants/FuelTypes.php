<?php

namespace DealTrak\Model\Constants;

class FuelTypes
{
    const PETROL = 1;
    const DIESEL = 2;
    const LPG = 3;
    const BI_FUEL = 3;
    const GAS = 4;
    const ELECTRIC = 5;
    const HYBRID = 6;  // Petrol/Electric
    const PETROL_BIO_ETHANOL = 7;
    const DIESEL_ELECTRIC = 8;
    const PETROL_PLUGIN_ELECTRIC = 9;
    const DIESEL_PLUGIN_ELECTRIC = 10;
}
