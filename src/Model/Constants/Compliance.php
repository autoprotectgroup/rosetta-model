<?php

namespace DealTrak\Model\Constants;

/**
 * Class Compliance.
 *
 * @package DealTrak\Model\Constants
 */
class Compliance
{
    public const ADMIN_GUI_URL = 'https://compliance.dealtrak123.co.uk';

    public const QUESTION_SET_TYPE_IDENTIFICATION = 1;
    public const QUESTION_SET_TYPE_STATUS_DISCLOSURE = 2;
    public const QUESTION_SET_TYPE_DEMANDS_AND_NEEDS = 3;
    public const QUESTION_SET_TYPE_CONSUMER_CREDIT = 4;
    public const QUESTION_SET_TYPE_GENERAL_INSURANCE_PRODUCT_INFORMATION = 5;
    public const QUESTION_SET_TYPE_DATA_PROTECTION = 6;
    public const QUESTION_SET_TYPE_PRODUCT_RECOMMENDATION = 7;
    public const QUESTION_SET_TYPE_GENERAL_INFORMATION = 8;
    public const QUESTION_SET_TYPE_PURCHASE_DECISION = 9;
    public const QUESTION_SET_TYPE_INSURANCE_PRESCRIBED_INFORMATION = 10;
    public const QUESTION_SET_TYPE_DEFERRED_SALE_OPT_OUT = 11;

    public const PRODUCT_TYPE_CATEGORY_VAP = 1;
    public const PRODUCT_TYPE_CATEGORY_FINANCE = 2;

    public const PRODUCT_TYPE_BREAKDOWN_COVER = 1;
    public const PRODUCT_TYPE_COMBINED_GAP_AND_VRI = 2;
    public const PRODUCT_TYPE_DEPOSIT_LOAN = 3;
    public const PRODUCT_TYPE_FINANCE_GAP = 4;
    public const PRODUCT_TYPE_GUARANTEE = 5;
    public const PRODUCT_TYPE_KEY_INSURANCE = 6;
    public const PRODUCT_TYPE_MOT_INSURANCE = 7;
    public const PRODUCT_TYPE_PAINT_PROTECTION = 8;
    public const PRODUCT_TYPE_SERVICE_PLAN = 9;
    public const PRODUCT_TYPE_SMART_INSURANCE = 10;
    public const PRODUCT_TYPE_TYRE_INSURANCE = 11;
    public const PRODUCT_TYPE_WARRANTY = 12;
    public const PRODUCT_TYPE_BACK_SOLD_PRODUCT = 13;
    public const PRODUCT_TYPE_CASH = 14;
    public const PRODUCT_TYPE_CONTRACT_HIRE = 15;
    public const PRODUCT_TYPE_DIRECT_LOAN = 16;
    public const PRODUCT_TYPE_EQUITY_RELEASE = 17;
    public const PRODUCT_TYPE_FINANCE_LEASE = 18;
    public const PRODUCT_TYPE_HIRE_PURCHASE = 19;
    public const PRODUCT_TYPE_LEASE = 20;
    public const PRODUCT_TYPE_LEASE_PURCHASE = 21;
    public const PRODUCT_TYPE_MANUFACTURERS_SCHEME = 22;
    public const PRODUCT_TYPE_MOTIBILITY_SCHEME = 23;
    public const PRODUCT_TYPE_NON_QUALIFIER = 24;
    public const PRODUCT_TYPE_PCP = 25;
    public const PRODUCT_TYPE_SAVINGS = 26;
    public const PRODUCT_TYPE_SECURED_LOAN = 27;
    public const PRODUCT_TYPE_TOP_UP_LOAN = 28;
    public const PRODUCT_TYPE_VARIABLE_RATE = 29;

    public const ELIGIBILITY_TYPE_NOT_APPLICABLE = 1;
    public const ELIGIBILITY_TYPE_ELIGIBLE = 2;
    public const ELIGIBILITY_TYPE_RECOMMENDED = 3;
    public const ELIGIBILITY_TYPE_NOT_RECOMMENDED = 4;
    public const ELIGIBILITY_TYPE_NOT_ELIGIBLE = 5;
    public const ELIGIBILITY_TYPE_NOT_APPLICABLE_STRING = 'Not Applicable';
    public const ELIGIBILITY_TYPE_ELIGIBLE_STRING = 'Eligible';
    public const ELIGIBILITY_TYPE_RECOMMENDED_STRING = 'Recommended';
    public const ELIGIBILITY_TYPE_NOT_RECOMMENDED_STRING = 'Not Recommended';
    public const ELIGIBILITY_TYPE_NOT_ELIGIBLE_STRING = 'Not Eligible';

    public const INSURANCE_HANDLE = 'insurance';
    public const FINANCE_HANDLE = 'finance';
}
