<?php

namespace DealTrak\Model\Constants;

class CommunicationTypes
{
    const SMS = 1;
    const EMAIL = 2;
    const FAX = 3;
    const LETTER = 4;
    const PDF = 5;
}
