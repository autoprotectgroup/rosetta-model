<?php

namespace DealTrak\Model\Constants;

class AutoSendActionStates
{
    const NONE = 0;
    const WAITING_FOR_VALIDATION = 1;
    const WAITING_FOR_SEND = 2;
    const WAITING_FOR_RESEND = 3;
    const SEND_ERROR = 4;
    const WAITING_FOR_DECISION = 5;
    const GOTO_NEXT_ACTION = 6;
    const RULE_COMPLETED = 7;
    const WAITING_FOR_MANUAL_DECISION = 8;
}
