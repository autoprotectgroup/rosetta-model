<?php

namespace DealTrak\Model\Constants;

class ResidentialStatus
{
    const HOME_OWNER = 1;
    const CO_HOME_OWNER = 2;
    const COUNCIL_TENANT = 3;
    const HOUSING_ASSOCIATION = 4;
    const LIVING_WITH_PARENTS = 5;
    const PRIVATE_TENANT = 6;
}
