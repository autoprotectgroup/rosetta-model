<?php

namespace DealTrak\Model\Constants;

class ProposalFlow
{
    const VEHICLE_FIRST = 1;
    const CUSTOMER_FIRST = 2;
}