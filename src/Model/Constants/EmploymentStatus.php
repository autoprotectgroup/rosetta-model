<?php

namespace DealTrak\Model\Constants;

class EmploymentStatus
{
    const FULL_TIME_PERMANENT = 1;
    const PART_TIME_PERMANENT = 2;
    const FULL_TIME_TEMPORARY = 3;
    const PART_TIME_TEMPORARY = 4;
    const FULL_TIME_AGENCY = 5;
    const PART_TIME_AGENCY = 6;
    const SUB_CONTRACTOR = 7;
    const SELF_EMPLOYED = 8;
    const UNEMPLOYED = 9;
    const RETIRED = 10;
    const STUDENT = 11;
    const HOUSE_WIFE = 12;
    const UNABLE_TO_WORK = 13;
}
