<?php

namespace DealTrak\Model\Constants;

class BusinessBuildingTenure
{
    const LEASED = 1;
    const LICENSED = 2;
    const MORTGAGED = 3;
    const OWNED = 4;
}
