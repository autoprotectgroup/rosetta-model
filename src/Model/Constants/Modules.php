<?php

namespace DealTrak\Model\Constants;

class Modules
{
    const FINANCE_OPTIMISER = 1;
    const ORDER_FORM = 2;
    const DATA_CHECKS = 3;
    const PAY_PLAN = 4;
    const QUICK_QUOTE = 5;
    const CSV_IMPORT = 6;
    const FUEL_SAVINGS_CALC = 7;
    const GAP_SALES_AID = 8;
    const EXTENDED_CASH_DEALS = 9;
    const FOURSQUARE = 10;
    const QUICK_QUOTE_2 = 11;
    const HPI_AVIVA_INSURANCE = 12;
    const DRIVEAWAY_INSURANCE = 12;
    const PRINTABLE_CARCHECK = 13;
    const ESIGNATURE = 14;
    const GLASS_VALUATION = 15;
    const IN_CAR_PRESENTATION = 16;
    const LP_QUOTE_COMPARE = 17;
    const EQUIFAX_AML = 18;
    const PCP_QUOTE_COMPARE = 19;
    const GLASS_QUICK_VALUATION = 20;
    const LOGIN_QUIZ = 21;
    const UNLOCK_SCHEDULE = 22;
    const DIARY = 23;
    const GAP_SALES_AID_3 = 25;
    const USER_ACCESS = 26;
    const TASKS = 27;
    const LENDER_CONDITIONS = 28;
    const SCRIPTS = 29;
    const INVOICES = 30;
    const CUSTOM_PROPOSAL_STATUSES = 31;
    const DEALER_COMMUNICATION = 32;
    const PRINTABLE_CARCHECK_HPI = 33;
    const IMPORT_DEALS_FROM_CSV = 34;
    const LEADS = 35;
    const SALESFORCE = 36;
    const COMPLIANCE = 37;
    const TRANSACTIONAL_BILLING = 38;
}
