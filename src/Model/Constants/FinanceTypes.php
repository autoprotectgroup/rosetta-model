<?php

namespace DealTrak\Model\Constants;

class FinanceTypes
{
    const HIRE_PURCHASE = 1;
    const PCP = 2;
    const MOTOR_LOAN = 3;
    const LEASE_PURCHASE = 4;
    const SAVINGS = 6;
    const EQUITY_RELEASE = 7;
    const SECURED_LOAN = 8;
    const PERSONAL_LOAN = 9;
    const MANUFACTURER_SCHEME = 10;
    const CONTRACT_HIRE = 11;
    const LEASE = 12;
    const FINANCE_LEASE = 13;
    const CASH = 14;
    const MOTABILITY = 15;
    const VARIABLE_RATE = 16;
    const TOP_UP_LOAN = 17;
    const FAST_PROPOSAL = 18;
    const NON_QUALIFIER = 19;
    const BACK_SALE = 20;

    const HP_MIN_TERM = 12;
    const HP_MAX_TERM = 60;
    const PCP_MIN_TERM = 12;
    const PCP_MAX_TERM = 60;
    const LP_MIN_TERM = 12;
    const LP_MAX_TERM = 60;
    const FL_MIN_TERM = 12;
    const FL_MAX_TERM = 60;
}
