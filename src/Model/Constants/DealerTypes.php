<?php

namespace DealTrak\Model\Constants;

class DealerTypes
{
    const FRANCHISE = 1;
    const USED_CARS = 2;
    const CAR_SUPERMARKET = 3;
    const OTHER = 4;
}
