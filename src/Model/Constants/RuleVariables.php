<?php

namespace DealTrak\Model\Constants;

class RuleVariables
{
    const DEALERSHIP = 1;
    const VEHICLE_NEW_OR_USED = 2;
    const HIRER_AGE = 3;
    const FINANCE_FLAT_RATE = 4;
    const FINANCE_TERM = 5;
    const FINANCE_BALANCE_TO_FINANCE = 6;
    const FINANCE_CREDIT_SCORE = 7;
    const FINANCE_LOAN_TO_VALUE = 8;
    const VEHICLE_AGE = 9;
    const HIRER_ANNUAL_GROSS_INCOME = 10;
    const FINANCE_GLASSES_LOAN_TO_VALUE = 11;
    const LICENCE_TYPE = 12;
    const FINANCE_TYPE = 13;
    const ASSOCIATED_PROPOSAL_DATA = 14;
    const NEGATIVE_EQUITY_BALANCE = 15;
    const NEGATIVE_EQUITY_DEPOSIT = 16;
    const FINANCE_TOTAL_DEPOSIT = 17;
    const DAY_OF_THE_WEEK = 18;
    const YEAR_OF_MANUFACTURE = 19;
    const VEHICLE_AGE_AT_END_OF_TERM = 20;
    const MILEAGE = 21;
    const VEHICLE_TYPE = 22;
    const PART_EXCHANGE = 23;
    const FINANCE_APR_RATE = 24;
    const DEALER_GROUP = 25;
    const TRANSMISSION_COUNT_MOD_4 = 26;
    const NUMBER_OF_TRANSACTIONS = 27;
    const LENDER_DECLINE = 28;
    const TRANSMISSION_COUNT_MOD_3 = 29;
    const BROKER_INTRODUCER = 30;
    const INTRODUCER = 31;
    const TODAY = 32;
    const TOMORROW = 33;
    const YESTERDAY = 34;
    const TIME = 35;
    const SOURCE_TYPE = 36;
    const ASSIGNED_DEALER = 37;
}
