<?php

namespace DealTrak\Model\Constants;

class InvoicesTypes
{
    const PART_EX = 1;
    const DEPOSIT = 2;
    const VEHICLE = 3;
    const COMMISSION = 4;
    const END_OF_LEASE = 5;
}
