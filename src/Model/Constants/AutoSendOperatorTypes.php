<?php

namespace DealTrak\Model\Constants;

class AutoSendOperatorTypes
{
    const OPERATOR_IS = 1;
    const OPERATOR_IS_NOT = 2;
    const OPERATOR_GREATER_THAN = 3;
    const OPERATOR_GREATER_THAN_OR_EQUAL_TO = 4;
    const OPERATOR_LESS_THAN = 5;
    const OPERATOR_LESS_THAN_OR_EQUAL_TO = 6;
    const OPERATOR_FOR = 7;
    const OPERATOR_NOT_FOR = 8;
}