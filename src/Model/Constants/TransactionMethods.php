<?php

namespace DealTrak\Model\Constants;

class TransactionMethods
{
    const METHOD_XML = 1;
    const METHOD_EMAIL = 2;
    const METHOD_PRINT = 3;
    const METHOD_EXCEL = 4;
}
