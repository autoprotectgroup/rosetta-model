<?php

namespace DealTrak\Model\Constants;

class EmploymentSector
{
    const SECTOR_PRIVATE = 1;
    const SECTOR_GOVERNMENT = 2;
    const SECTOR_MILITARY = 3;
    const SECTOR_SELF_EMPLOYED = 4;
    const SECTOR_STUDENT = 5;
    const SECTOR_HOME_MAKER = 6;
    const SECTOR_RETIRED = 7;
    const SECTOR_UNABLE_TO_WORK = 8;
    const SECTOR_UNEMPLOYED = 9;
}
