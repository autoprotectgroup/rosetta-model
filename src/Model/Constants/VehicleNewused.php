<?php

namespace DealTrak\Model\Constants;

/**
 * Class VehicleNewused.
 *
 * @package DealTrak\Model\Constants
 */
class VehicleNewused
{
    const IS_NEW = 1;
    const IS_USED = 2;
    const CREDIT_LINE = 3;
    const PRE_REGISTERED = 4;
    const EX_DEMONSTRATOR = 5;
    const NO = 6;
}
