<?php

namespace DealTrak\Model\Constants;

class ProposalStatus
{
    const NEW_PROPOSAL = 1;
    const QUOTATION = 2;    // Unsent Proposal
    const AWAITING_DECISION = 3;
    const AWAITING_DELIVERY = 4;
    const DOCS_SENT_TO_LENDER = 5;
    const DOCUMENT_ERROR = 6;
    const PAID_OUT = 7;
    const DECLINED_ALL_LENDERS = 8;
    const CANCELLED = 9;
    const NOT_TAKEN_UP = 10;
    const REFERRED_TO_BROKER = 11;
    const AWAITING_INVOICE = 12;
    const AWAITING_QUOTE = 13;
    const PAID_OUT_NOT_DELIVERED = 14;
    const DOCS_SENT_TO_CUSTOMER = 15;
    const NEW_ORDER = 16;
    const DELIVERED_NOT_PAID_OUT = 17;
    const SOURCING_VEHICLE = 18;
    const UNABLE_TO_ASSIST = 19;
    const AWAITING_DEALER = 21;
    const AWAITING_CUSTOMER = 22;
    const APPOINTED_AT_DEALERSHIP = 23;
    const AWAITING_INCOME_AND_EXPENDITURE = 24;
    const AWAITING_CUSTOMER_CONTACT = 25;
    const LOAN_PROVISIONALLY_APPROVED = 26;
}
