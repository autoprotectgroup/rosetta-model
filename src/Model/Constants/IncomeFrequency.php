<?php

namespace DealTrak\Model\Constants;

class IncomeFrequency
{
    const WEEKLY = 1;
    const FORTNIGHTLY = 2;
    const FOUR_WEEKLY = 3;
    const MONTHLY = 4;
    const QUARTERLY = 5;
    const ANNUALLY = 6;
}
