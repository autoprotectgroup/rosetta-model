<?php

namespace DealTrak\Model\Constants;

class BrokerDealerInformationStatus
{
    const LIVE = 1;
    const WARM_PROSPECT = 2;
    const INACTIVE = 3;
    const COLD_PROSPECT = 4;
    const BANNED = 5;
    const DELETED = 6;
}
