<?php

namespace DealTrak\Model\Constants;

class ApplicantTypes
{
    const HIRER = 1;
    const JOINT = 2;
    const GUARANTOR = 3;
}
