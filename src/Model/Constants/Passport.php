<?php

namespace DealTrak\Model\Constants;

class Passport
{
    const UK_PASSPORT = 1;
    const UK_CITIZEN_NO_PASSPORT = 2;
    const EU_PASSPORT = 3;
    const OTHER = 4;
}
