<?php

namespace DealTrak\Model\Constants;

class OrganisationTypes
{
    const PRIVATE_INDIVIDUAL = 0;
    const PRIVATE_LIMITED_COMPANY = 1;
    const PUBLIC_LIMITED_COMPANY = 2;
    const PARTNERSHIP = 3;
    const SOLE_TRADER = 4;
    const OTHER = 5;
    const LLP = 6;
}
