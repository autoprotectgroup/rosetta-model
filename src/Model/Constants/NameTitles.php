<?php

namespace DealTrak\Model\Constants;

class NameTitles
{
    const MR = 1;
    const MRS = 2;
    const MS = 3;
    const MISS = 4;
    const DR = 5;
}
