<?php

namespace DealTrak\Model\Constants;

class VehicleTypes
{
    const PRIVATE_LIGHT_GOODS_VEHICLE = 1;
    const LIGHT_COMMERCIAL_VEHICLE = 2;
    const CARAVAN = 3;
    const MOTORHOME = 4;
    const MOTORBIKE = 5;
    const VEHICLE_OTHER = 6;
    const HEAVY_GOODS_VEHICLE = 7;
}
