<?php

namespace DealTrak\Model\Constants;

class OperatorTypes
{
    const EQUALS = 1;
    const NOT_EQUAL = 2;
    const GREATER_THAN = 3;
    const GREATER_THAN_OR_EQUAL_TO = 4;
    const LESS_THAN = 5;
    const LESS_THAN_OR_EQUAL_TO = 6;
    const IN_LIST = 7;
    const NOT_IN_LIST = 8;
    const CONTAINS = 9;
    const NOT_NULL = 10;
    const IS = 11;
    const IS_NOT = 12;
    const DOES_NOT_CONTAIN = 13;
}
