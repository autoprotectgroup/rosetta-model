<?php

namespace DealTrak\Model\Constants;

class DrivingLicense
{
    const FULL = 1;
    const NON_UK = 2;
    const PROVISIONAL = 3;
    const NONE = 4;
    const EU = 5;
}
