<?php

namespace DealTrak\Model\Constants;

class MaritalStatus
{
    const SINGLE = 1;
    const MARRIED = 2;
    const COMMON_LAW = 3;
    const CIVIL_PARTNERS = 4;
    const SEPARATED = 5;
    const DIVORCED = 6;
    const WIDOWED = 7;
    const UNKNOWN = 8;
}
