<?php

namespace DealTrak\Model\Constants;

class ValidationVariables
{
    const DEALERSHIP = 1;
    const VEHICLE_NEW_OR_USED = 2;
    const HIRER_AGE = 3;
    const FINANCE_FLAT_RATE = 4;
    const FINANCE_TERM = 5;
    const FINANCE_BALANCE_TO_FINANCE = 6;
    const FINANCE_CREDIT_SCORE = 7;
    const FINANCE_LOAN_TO_VALUE = 8;
    const VEHICLE_AGE = 9;
    const HIRER_ANNUAL_GROSS_INCOME = 10;
    const FINANCE_GLASSES_LOAN_TO_VALUE = 11;
    const LICENCE_TYPE = 12;
    const FINANCE_TYPE = 13;
    const FINANCE_PACKAGE = 14;
    const FINANCE_TOTAL_DEPOSIT = 15;
    const JOINT_AGE = 16;
    const YEAR_OF_MANUFACTURE = 17;
    const VEHICLE_AGE_AT_END_OF_TERM = 18;
    const MILEAGE = 19;
    const VAP_PRODUCTS = 20;
    const VEHICLE_PRICE = 21;
    const DRIVING_LICENCE_NUMBER = 22;
    const PROPOSAL_TYPE = 23;
    const MOBILE_NUMBER = 24;
    const FINANCE_PACKAGE_BY_ID = 25;
    const FINANCE_APR = 26;
    const HIRER_EMAIL = 27;
    const HIRER_HOME_PHONE = 28;
    const HIRER_MOBILE = 29;
    const HIRER_WORK_PHONE = 30;
    const JOINT_EMAIL = 31;
    const JOINT_HOME_PHONE = 32;
    const JOINT_MOBILE = 33;
    const JOINT_WORK_PHONE = 34;
    const VEHICLE_TYPE = 35;
    const HIRER_ANNUAL_NET_INCOME = 36;
    const IRR = 37;
    const EMPLOYMENT = 38;
    const HIRER_AGE_AT_END_OF_TERM = 39;
    const HIRER_MONTHLY_NET_INCOME = 40;
    const HIRER_MONTHLY_GROSS_INCOME = 41;
    const LENDER_DECLINE = 42;
    const TIME_AT_CURRENT_EMPLOYMENT = 43;
    const EMPLOYMENTS_IN_LAST_12_MONTHS = 44;
    const ON_TRADE_PREMISES = 45;
    const TIME_AT_CURRENT_BANK = 46;
    const EMPLOYMENT_SECTOR = 47;
    const OCCUPATION = 48;
    const BANK_VALID = 49;
    const HIRER_NATIONALITY = 50;
    const PERSONAL_HIRER_TYPE = 51;
    const MAX_DECLINES_NUMBER = 52;
    const DEALER_GROUP = 53;
    const PART_EXCHANGE = 54;
}
