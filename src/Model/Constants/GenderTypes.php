<?php

namespace DealTrak\Model\Constants;

class GenderTypes
{
    const MALE = 1;
    const FEMALE = 2;
}
