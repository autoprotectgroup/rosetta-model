<?php

namespace DealTrak\Model\Constants;

class VapTypes
{
    const OTHER = 1; // Old VAP
    const FINANCE_GAP = 2;
    const WARRANTY = 3;
    const MOT_INSURANCE = 4;
    const TYRE_INSURANCE = 5;
    const KEY_INSURANCE = 6;
    const RTI_GAP = 7;
    const BREAKDOWN_COVER = 8;
    const SMART_INSURANCE = 9;
    const COMBINED_GAP_VRI = 10;
    const GUARANTEE = 11;
    const PAINT_INSURANCE = 12;
    const ROADSIDE_INSURANCE = 13;
    const DEALER_MISC_PRODUCT = 14;
    const SERVICE_PLAN = 15;
    const ALLOY_WHEEL_PROTECTION = 16;
    const CARS_PLUS_PROTECTION = 17;
    const EXCESS = 18;
    const RTI = 19;
    const VRI = 20;
    const MOT_GUARANTEE = 21;
    const RECOVERY_GUARANTEE = 22;
    const MOTOR_INSURANCE = 23;

    const TAX_TYPE_IPT = 'IPT';
    const TAX_TYPE_VAT = 'VAT';
    const TAX_TYPE_ZERO_RATED = 'Zero Rated';
}
