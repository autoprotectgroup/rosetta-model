<?php

namespace DealTrak\Model\Constants;

class CommunicationReasons
{
    const SERVICE = 1;
    const MARKETING = 2;
}
