<?php

namespace DealTrak\Model\Constants;

class InsuranceTypes
{
    const LIFE = 1;
    const ACCIDENT = 2;
    const SICKNESS = 3;
    const UNEMPLOYMENT = 4;
    const CRITICAL_ILLNESS = 5;
}
