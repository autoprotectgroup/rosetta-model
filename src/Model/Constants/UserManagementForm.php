<?php

namespace DealTrak\Model\Constants;

class UserManagementForm
{
    public const ADD_ALL_DEALERS = 0;
    public const ADD_ALL_GROUPS = 0;
    public const DEFAULT_PAGE_NUMBER = 1;
    public const DEFAULT_ADMIN_FILTER = true;
    public const DEFAULT_PAGINATION_LIMIT = 10;
    public const DEFAULT_ORDER_BY_SORT = 'ASC';
    public const DEFAULT_ORDER_BY_SORT_LOGS = 'DESC';
    public const DEFAULT_SEARCH = '';
    public const DEFAULT_SEARCH_FOR = null;
}
