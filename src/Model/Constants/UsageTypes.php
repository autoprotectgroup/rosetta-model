<?php

namespace DealTrak\Model\Constants;

class UsageTypes
{
    const BUSINESS = 1;
    const DRIVING_SCHOOL = 2;
    const SOCIAL = 3;
    const TAXI = 4;
}
