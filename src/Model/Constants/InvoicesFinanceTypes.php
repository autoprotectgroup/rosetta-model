<?php

namespace DealTrak\Model\Constants;

class InvoicesFinanceTypes
{
    const VEHICLE = 1;
    const DEPOSIT = 2;
    const PART_EX = 3;
    const CUSTOM = 4;
    const FINANCE_COMMISSION = 5;
    const GAP = 6;
    const VOLUME_BONUS = 7;
    const DOC_FEE = 8;
    const SELF_INVOICE = 9;
    const FRONT_END_PROFIT = 10;
    const BACK_END_LOSS = 11;
    const TT_CHARGE = 12;
    const OPTIONS = 13;
    const ACCESSORIES = 14;
    const DELIVERY = 15;
    const RFL = 16;
    const REG_FEE = 17;
    const SETTLEMENT = 18;
    const DEPOSIT_STERLING = 19;
    const TITLE_CHARGE = 20;
    const AGREEMENT_NUMBER = 21;
    const ASSET_DETAILS = 22;
}
