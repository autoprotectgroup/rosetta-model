<?php

namespace DealTrak\Model\Constants;

/**
 * Class DealTrak.
 *
 * For constants relating to our company to ensure continuity on pages and in the system.
 */
class DealTrak
{
    const COMPANY_NAME = 'DealTrak';
    const SYSTEM_NAME = 'Dealtrak123';
    const SUPPORT_EMAIL = 'help@dealtrak.co.uk';
    const HELP_DESK_URL = 'https://help.dealtrak.co.uk';
    const NO_REPLY_EMAIL = 'no-reply@dealtrak.co.uk';
    const FINANCE_EMAILS = [
//        'joanna.burton@dealtrak.co.uk',
        'chris.bailey@dealtrak.co.uk',
    ];
}
