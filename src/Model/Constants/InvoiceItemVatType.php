<?php

namespace DealTrak\Model\Constants;

class InvoiceItemVatType
{
    const STANDARD = 1;
    const EXEMPT = 2;
    const REDUCED = 3;
    const ZERO_RATE = 4;
    const OUT_OF_SCOPE = 5;
}
