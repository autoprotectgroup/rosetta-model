<?php

require_once 'vendor/autoload.php';

use DealTrak\Model\Constants\ApplicantTypes;
use DealTrak\Model\Constants\Destinations;
use DealTrak\Model\Constants\DrivingLicense;
use DealTrak\Model\Constants\EmploymentSector;
use DealTrak\Model\Constants\EmploymentStatus;
use DealTrak\Model\Constants\FinanceTypes;
use DealTrak\Model\Constants\FuelTypes;
use DealTrak\Model\Constants\GenderTypes;
use DealTrak\Model\Constants\MaritalStatus;
use DealTrak\Model\Constants\NameTitles;
use DealTrak\Model\Constants\Nationality;
use DealTrak\Model\Constants\Passport;
use DealTrak\Model\Constants\RelationshipTypes;
use DealTrak\Model\Constants\ResidentialStatus;
use DealTrak\Model\Constants\UsageTypes;
use DealTrak\Model\Constants\VehicleNewused;
use DealTrak\Model\Constants\VehicleTypes;
use DealTrak\Model\Rosetta\EmploymentSection;
use DealTrak\Model\Rosetta\ExtrasSection;
use DealTrak\Model\Rosetta\FinanceSection;
use DealTrak\Model\Rosetta\Personal\AffordabilitySection;
use DealTrak\Model\Rosetta\Personal\ApplicantDetailsSection;
use DealTrak\Model\Rosetta\Personal\ApplicantSection;
use DealTrak\Model\Rosetta\Personal\Application;
use DealTrak\Model\Rosetta\Personal\BankSection;
use DealTrak\Model\Rosetta\ResidenceSection;
use DealTrak\Model\Rosetta\Rosetta;
use DealTrak\Model\Rosetta\Routing;
use DealTrak\Model\Rosetta\VehicleSection;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;

Doctrine\Common\Annotations\AnnotationRegistry::registerLoader('class_exists');

$serializer = SerializerBuilder::create()->build();

/****
 ** Example of loading an existing Rosetta XML document into the model
 ***/

$doc = new \DOMDocument();
$doc->load('rosetta_personal_example.xml');
$xml = $doc->saveXML();

$rosetta = $serializer->deserialize($xml, Rosetta::class, 'xml');

print_r($rosetta);

/****
 ** Example of building a personal application Rosetta model object and deserializing it to XML.
 ***/

$rosetta = new Rosetta(
    1,                                         // $live (is the application live or test)
    new Routing(
        Destinations::DESTINATION_BLACKHORSE,  // $destination (ID)
        12345,                                 // $client (ID)
        '123456',                                // $branchId  (dealer)
        'DealTrak',                            // $contactForename
        'Support',                             // $contactSurname
        '01424853764',                         // $contactPhone
        'support@dealtrak.co.uk',              // $contactEmail
        '58543004',                            // $identifier (dealer lender identifier)
        'test.user',                           // $username (lender auth)
        'test.pass',                           // $password (lender auth)
        '4354-GF45-FG85-KK85',                 // $clientReference
        '2019-06-10 11:37:17',                 // $proposalCreatedAt
        '2019-06-21T18:03:17+01:00',            // $createdAt  (yes, it's a different time format!)
        '6219562',                             // $rosettaReference
        ''                                   // $sourceReference
    ),
    null,                                      // $actions
    new Application(                           // $application
        [
            new ApplicantSection(              // $applicantSection
                1,                             // $sequence
                ApplicantTypes::HIRER,         // $type
                null,                          // $soleTrader
                new ApplicantDetailsSection(
                    NameTitles::MR,              // $title
                    'Darren',                    // $forename
                    'Wright',                    // $surname
                    GenderTypes::MALE,           // $gender
                    '07798266031',               // $homePhone
                    0,                           // $dependants
                    MaritalStatus::SINGLE,       // $maritalStatus
                    Nationality::UNITED_KINGDOM, // $nationality
                    0,                           // $distanceMarketed
                    0,                           // $optinPost
                    0,                           // $optinEmail
                    0,                           // $optinPhone
                    0,                           // $optinSms
                    1,                           // $ukResident
                    '',                          // $middlename
                    '',                          // $alias
                    '',                          // $maidenName
                    '1985-07-01',                // $dob
                    '07256643462',               // $mobile
                    '07334622587',               // $workPhone
                    'joe.bloggs@somewhere.com',  // $email
                    Passport::UK_PASSPORT,       // $passport  (type)
                    DrivingLicense::FULL,        // $drivingLicense
                    12345678,                    // $drivingLicenseNumber
                    RelationshipTypes::PARTNER,  // $relationship
                    null,                        // $pension
                    492,                         // $creditEnquiry
                    null                         // $aml
                ),
                [
                    new ResidenceSection(
                        1,                                 // $sequence
                        'Wood Ports',                      // $street
                        'Leeds',                           // $townCity
                        'West Yorkshire',                  // $county
                        'LS1 2PG',                         // $postcode
                        6,                                 // $years
                        0,                                 // $months
                        ResidentialStatus::PRIVATE_TENANT, // $residentialStatus
                        1,                                  // $ukAddress
                        '',                                // $flat
                        '',                                // $houseName
                        '12',                              // $houseNumber
                        ''                                // $district
                    ),
                ],
                [
                    new EmploymentSection(
                        1,                                      // $sequence
                        'Programmer',                           // $occupation
                        12,                                     // $occupationId
                        'DealTrak',                             // $company
                        'Block F',                              // $building
                        'Leeds',                                // $townCity
                        4,                                      // $years
                        9,                                      // $months
                        EmploymentStatus::FULL_TIME_PERMANENT,  // $employmentStatus
                        EmploymentSector::SECTOR_PRIVATE,       // $employmentSector
                        0,                                      // $employmentType
                        12,                                     // $incomeFreq
                        1,                                       // $ukTradingAddress
                        'Leeds Dock',                           // $subBuilding
                        'The Boulevard',                        // $street
                        '',                                     // $district
                        'West Yorkshire',                       // $county
                        'LS10 1LR',                             // $postcode
                        '33333.33',                             // $grossIncome
                        '33333.33',                             // $income
                    ),
                ],
                new AffordabilitySection(
                    0,                                          // $replacementLoan
                    0,                                          // $sustainability
                    500,                                        // $monthlyMortgage
                    0                                           // $otherExpenditure
                ),
                new BankSection(
                    'Mr. Frank Ross',                            // $accountName
                    '10452861',                                  // $accountNumber
                    '09-01-26',                                  // $sortCode
                    'Santander UK Plc',                          // $bankName
                    'Bootle',                                    // $branch
                    'Bridle Road, Bootle, Merseyside, L30 4GB',  // $address
                    10,                                          // $years
                    0,                                           // $months
                    0,                                           // $amex
                    0,                                           // $chequeCard
                    0,                                           // $dinersClub
                    0,                                           // $masterCard
                    0,                                           // $visa
                    1,                                           // $debitCard
                    0,                                           // $other
                    0                                            // $directDebit
                ),
            ),
        ],
        new VehicleSection(
            VehicleNewused::IS_USED,                             // $newUsed
            'Volkswagen',                                        // $make
            'Golf',                                              // $model
            1600,                                                // $engineSize
            'Match Tdi',                                         // $style
            1,                                                   // $assetTypeId
            'Golf Match 1.6 Tdi',                                // $description
            5,                                                   // $doors
            'WT4184001',                                         // $regNo
            '2015-01-01',                                        // $firstReg
            'WVWZZZAUZFW093823',                                 // $vin
            'VWGO16MAT5HDTM  5',                                 // $capCode
            '10000.00',                                          // $tradeValue
            '11000.00',                                          // $retailValue
            '197750',                                            // $glassesCode
            '001',                                               // $glassesCodeFamily
            'Black',                                             // $colour
            FuelTypes::DIESEL,                                   // $fuel
            '6 Speed Manual Diesel',                             // $transmission
            UsageTypes::SOCIAL,                                  // $usage
            '2014',                                              // $yearMan
            0,                                                   // $import
            56124,                                               // $mileage
            0,                                                   // $stockNumber
            '2019-04-03',                                        // $deliveryDate
            VehicleTypes::PRIVATE_LIGHT_GOODS_VEHICLE            // $type
        ),
        null,
        new FinanceSection(
            FinanceTypes::HIRE_PURCHASE,                         // $financeType
            0,                                                   // $vatQualifying
            'APR',                                               // $interestMethod
            '0',                                                 // $acceptanceFee
            'up front',                                          // $acceptanceMethod
            '1',                                                 // $acceptanceFeeInterestCharged
            '0',                                                 // $closureFee
            'at end',                                            // $closureMethod
            '1',                                                 // $closureFeeInterestCharged
            '11000.00',                                          // $vehicle
            '0',                                                 // $vehicleVat
            '0',                                                 // $options
            '0',                                                 // $optionsVat
            '0',                                                 // $accessories
            '0',                                                 // $accessoriesVat
            '0',                                                 // $delivery
            '0',                                                 // $deliveryVat
            '0',                                                 // $guarantee
            '0',                                                 // $guaranteeVat
            '100.00',                                            // $rfl
            12,                                                  // $rflTerm
            '0',                                                 // $regFee
            '0',                                                 // $fhbr
            '',                                                  // $rob
            '',                                                  // $profile1
            '',                                                  // $profile2
            '',                                                  // $profile3
            '',                                                  // $profile1Payment
            '',                                                  // $profile2Payment
            '',                                                  // $profile3Payment
            '',                                                  // $monthlyPayment
            '',                                                  // $regDealType
            0,                                                   // $optedOut
            null,                                                // $optionsSection
            null,                                                // $accessoriesSection
            null,                                                // $vaps
            '0',                                                 // $partEx
            '0',                                                 // $settlement
            null,                                                // $settlementLender
            '1000.00',                                           // $deposit
            '0',                                                 // $depositSterling
            0,                                                   // $incomePromiseScheme,
            null,                                                // $fda
            'APR',                                               // $rateType
            '4.32',                                              // $flatRate
            '7.9',                                               // $aprRate
            60,                                                  // $term
            12,                                                  // $paymentFrequency
            null,                                                // $pauseType
            '0',                                                 // $balloon
            null,                                                // $annualMileage
            '20',                                                // $vatRate
            null,                                                // $cpi
            'HH5',                                               // $financePackage
            '123542',                                            // $financePackageId
            '0',                                                 // $ltv
            '0',                                                 // $ltvGlass
            '492'                                                // $creditScore
        ),
        new ExtrasSection(
            null,                                                // $notes
            null,                                                // $lenderNotes
            []                                                   // $data - extra lender data
        )
    )
);

$xml = $serializer->serialize(
    $rosetta,
    'xml',
    SerializationContext::create()->setGroups(['personal'])
);

print_r($xml);